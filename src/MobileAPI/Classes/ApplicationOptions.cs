﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Classes
{
    public class ApplicationOptions
    {
        public JwtSettings JwtSettings { get; set; }
        public BrandOptions BrandOptions { get; set; }
        public ReviewAPIOptions ReviewAPIOptions { get; set; }
        public AppAuthOptions AppAuthOptions { get; set; }
    }

    public class BrandOptions
    {
        public string LoginName { get; set; }
        public string Password { get; set; }
        public string PaymentRootUrl { get; set; }
        public string ParallelSystemsSecureKey { get; set; }
    }

    public class ReviewAPIOptions
    {
        public string Url { get; set; }
        public string Credential { get; set; }
    }

    public class AppAuthOptions
    {
        public string Key { get; set; }
    }

}
