﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MobileAPI.Constants;
using MobileAPI.Extensions;
using MobileAPI.Helpers;
using MobileAPI.Services;
using PS.BookingService;
using PS.ContactService;
using PS.GeneralService;
using System;
using System.Linq;

namespace MobileAPI.Classes
{
    public static class ProjectServiceCollectionExtensions
    {

        public static IServiceCollection AddProjectServices(this IServiceCollection services, IConfiguration configuration)
        {
            var privateServiceOptions = configuration.GetSection(nameof(PrivateServiceOptions)).Get<PrivateServiceOptions>();

            services.AddScoped<ITokenGenerator, TokenGenerator>()
            .AddHttpContextAccessor()
            .AddScoped(provider =>
            {
                return CreateBrandService(configuration, provider);

            })
            .AddScoped((Func<IServiceProvider, IBookingService>)(provider => new BookingServiceClient(BookingServiceClient.EndpointConfiguration.BasicHttpBinding_IBookingService, privateServiceOptions.BookingServiceUrl)))
            .AddScoped((Func<IServiceProvider, IContactService>)(p => new ContactServiceClient(ContactServiceClient.EndpointConfiguration.BasicHttpBinding_IContactService, privateServiceOptions.ContactServiceUrl)))
            .AddScoped((Func<IServiceProvider, IGeneral>)(p => new GeneralClient(GeneralClient.EndpointConfiguration.BasicHttpBinding_IGeneral, privateServiceOptions.GeneralServiceUrl)))
            .AddScoped<ILangService, LangService>()
            .AddScoped<IAuthService, AuthService>()
            .AddScoped<IContactManager, ContactService>()
            .AddScoped<IBookingManager, BookingService>();

            return services;
        }

        private static IBrandService CreateBrandService(IConfiguration configuration, IServiceProvider provider)
        {
            var httpContext = provider.GetRequiredService<IHttpContextAccessor>().HttpContext;

            var brand = httpContext.Request.Headers.Where(h => h.Key == "Brand" || h.Key == "brand")?.FirstOrDefault().Value.ToString().ToLowerWithReplace() ?? Brands.MijnNL;

            if (string.IsNullOrEmpty(brand))
                brand = Brands.MijnNL;


            var headerLanguageCode = string.Empty;
            try
            {
                //headerLanguageCode = httpContext.Request.Headers.Where(h => h.Key == "LanguageCode" || h.Key == "languageCode" || h.Key == "Languagecode" || h.Key == "languagecode")?.FirstOrDefault().Value.ToString().Split('-')[1] ?? LanguageCodes.NL;
                headerLanguageCode = httpContext.Request.Headers
                                    .Where(h => h.Key == "LanguageCode" || h.Key == "languageCode" || h.Key == "Languagecode" || h.Key == "languagecode")?
                                    .Select(l => l.Value.ToString().Contains('-') ? l.Value.ToString().Split('-')[1].ToUpperWithReplace() : l.Value.ToString().ToUpperWithReplace())?.FirstOrDefault() ?? LanguageCodes.NL;
            }
            catch (Exception ex)
            {
                headerLanguageCode = LanguageCodes.NL;
            }

            if (string.IsNullOrEmpty(headerLanguageCode))
                headerLanguageCode = LanguageCodes.NL;


            var languageCode = LanguageCodes.NL;
            if (headerLanguageCode == LanguageCodes.FR)
                languageCode = LanguageCodesOfSystem.FR_BE;


            var languageForBrand = BrandHelper.GetLanguageCodeByBrand(brand);

            var brandOptions = configuration.GetSection(brand).Get<BrandOptions>();

            var brandServiceObj = new BrandService
            {
                Brand = brand,
                BrandLanguage = languageForBrand,
                LanguageCodeForResponseMessages = languageCode,
                Options = brandOptions
            };

            return brandServiceObj;
        }
    }

    public class PrivateServiceOptions
    {
        public string BookingServiceUrl { get; set; }
        public string ContactServiceUrl { get; set; }
        public string GeneralServiceUrl { get; set; }
    }

    public static class CustomServiceCollectionExtensions
    {
        public static IServiceCollection AddCustomOptions(this IServiceCollection services, IConfiguration configuration) =>
            services.Configure<JwtSettings>(configuration.GetSection(nameof(ApplicationOptions.JwtSettings))).AddSingleton(x => x.GetRequiredService<IOptions<JwtSettings>>().Value)
            .Configure<ReviewAPIOptions>(configuration.GetSection(nameof(ApplicationOptions.ReviewAPIOptions))).AddSingleton(x => x.GetRequiredService<IOptions<ReviewAPIOptions>>().Value)
            .Configure<AppAuthOptions>(configuration.GetSection(nameof(ApplicationOptions.AppAuthOptions))).AddSingleton(x => x.GetRequiredService<IOptions<AppAuthOptions>>().Value);


    }
}
