﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Classes
{
    public class JwtSettings
    {
        public string SigningSecret { get; set; }
        public int ExpiryDuration { get; set; }
    }
}
