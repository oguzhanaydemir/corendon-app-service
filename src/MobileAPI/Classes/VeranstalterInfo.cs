﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Classes
{
    public class VeranstalterInfo
    {
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Veranstalter { get; set; }
    }
}
