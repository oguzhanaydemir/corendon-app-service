﻿using Microsoft.IdentityModel.Tokens;
using MobileAPI.Constants;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace MobileAPI.Classes
{
    public interface ITokenGenerator
    {
        //CorendonToken CreateToken(string username, string password);
        string CreateToken(string contacId);
        string ResolveToken(string token);
    }

    public class TokenGenerator : ITokenGenerator
    {
        private readonly JwtSettings _jwtSettings;

        public TokenGenerator(JwtSettings jwtSettings)
        {
            _jwtSettings = jwtSettings;
        }

        //public CorendonToken CreateToken(string contactId)
        //{
        //    var expireTime = DateTime.Now.AddMinutes(_jwtSettings.ExpiryDuration);
        //    var tokenHandler = new JwtSecurityTokenHandler();
        //    var key = Encoding.ASCII.GetBytes(_jwtSettings.SigningSecret);
        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(new Claim[]
        //        {
        //            new Claim(ClaimTypeContants.ContactId, contactId)
        //        }),
        //        Expires = expireTime,
        //        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
        //    };

        //    var token = tokenHandler.CreateToken(tokenDescriptor);
        //    string tokenString = tokenHandler.WriteToken(token);

        //    return new CorendonToken() { Token = tokenString, ExpireAt = expireTime };
        //}

        public string CreateToken(string contactId)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_jwtSettings.SigningSecret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypeContants.ContactId, contactId)
                }),
                Expires = DateTime.Now.AddMinutes(_jwtSettings.ExpiryDuration),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenResult = tokenHandler.CreateToken(tokenDescriptor);
            string token = tokenHandler.WriteToken(tokenResult);

            return token;
        }

        public string ResolveToken(string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_jwtSettings.SigningSecret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    ClockSkew = TimeSpan.Zero
                }, out SecurityToken validatedToken);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = jwtToken.Claims.First(x => x.Type == ClaimTypeContants.ContactId)?.Value;

                return userId;
            }
            catch (Exception ex)
            {
                var hede = ex;
            }

            return "";
        }
    }

    public class CorendonToken
    {
        public string Token { get; set; }
        public DateTime ExpireAt { get; set; }
    }
}
