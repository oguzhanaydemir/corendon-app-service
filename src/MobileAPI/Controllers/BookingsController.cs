﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MobileAPI.Constants;
using MobileAPI.Constants.RouteContants;
using MobileAPI.Models;
using MobileAPI.RequestModels;
using MobileAPI.Services;
using PS.BookingService;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace MobileAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class BookingsController : ControllerBase
    {
        private readonly IBookingManager _bookingService;

        public BookingsController(IBookingManager bookingService, IBrandService brandService)
        {
            _bookingService = bookingService;
        }
        /// <summary>
        /// Get booking detail by PNR
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("detail", Name = BookingsControllerRoute.Detail)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get booking detail by PNR", typeof(BookingDetailResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> BookingDetails(string id)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.GetDetail(Convert.ToInt32(contactId), id);
        }

        /// <summary>
        /// Get payment history of booking
        /// </summary>
        /// <param name="iparam"></param>
        /// <returns></returns>
        [HttpGet("paymenthistory", Name = BookingsControllerRoute.PaymentHistory)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(List<BookingPaymentsPaymentResponse>))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> PaymentHistory(string iparam)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.PaymentHistory(Convert.ToInt32(contactId), iparam);
        }

        /// <summary>
        /// Add booking to contact by surname and PNR informations.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("add", Name = BookingsControllerRoute.Add)]
        [SwaggerResponse(StatusCodes.Status200OK, "Add booking to contact by surname and PNR informations", typeof(AddBookingResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> AddBookingToContact(AddBookingRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _bookingService.AddBookingToContact(request);
        }

        /// <summary>
        /// Get hotel voucher of booking as pdf.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("voucher", Name = BookingsControllerRoute.GetVoucher)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(PDFResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetVoucher(string id)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.GetVoucher(Convert.ToInt32(contactId), id);
        }

        /// <summary>
        /// Get flight ticket of booking as pdf.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("ticket", Name = BookingsControllerRoute.GetTicket)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(PDFResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetTicket(string id)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.GetTicket(Convert.ToInt32(contactId), id);
        }

        /// <summary>
        /// Sends the invoice by email.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("invoicesend", Name = BookingsControllerRoute.InvoiceSendEmail)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(InvoiceSendEmailResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> InvoiceSendToEmail(string id)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.InvoiceSendToEmail(Convert.ToInt32(contactId), id);
        }

        /// <summary>
        /// Cancels the booking by PNR
        /// </summary>
        /// <param name="request">The request contains pnr, iban and bic.</param>
        /// <returns></returns>
        [HttpPost("cancel", Name = BookingsControllerRoute.CancelBooking)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(CancelBookingResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> CancelBooking(CancelBookingRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _bookingService.CancelBooking(request);
        }

        /// <summary>
        /// Returns birthdate validation result 
        /// </summary>
        /// <param name="request">The request contains pnr, surname and birthdate</param>
        /// <returns></returns>
        [HttpPost("check-pnr-privacy", Name = BookingsControllerRoute.CheckPnrPrivacy)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(CheckPnrPrivacyResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> CheckPnrPrivacy(CheckPnrPrivacyRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _bookingService.CheckPnrPrivacy(request);
        }

        /// <summary>
        /// It returns a url to the client to redirect to the review page.
        /// </summary>
        /// <param name="id">The id parameter must be PNR of booking</param>
        /// <returns></returns>
        [HttpGet("booking-review", Name = BookingsControllerRoute.Review)]
        [SwaggerResponse(StatusCodes.Status200OK, "Get payment history of booking", typeof(BookingReviewResponseModel))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> BookingReview(string id)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();

            return await _bookingService.BookingReview(Convert.ToInt32(contactId), id);
        }
    }
}
