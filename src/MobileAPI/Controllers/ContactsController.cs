﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.RequestModels;
using System.Threading.Tasks;
using MobileAPI.Services;
using MobileAPI.Constants.RouteContants;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Swashbuckle.AspNetCore.Annotations;
using MobileAPI.Models;
using System.Collections.Generic;
using PS.ContactService;
using MobileAPI.Constants;
using System;

namespace MobileAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize]
    public class ContactsController: ControllerBase
    {
        private readonly IContactManager _contactService;

        public ContactsController(IContactManager contactService)
        {
            _contactService = contactService;
        }

        /// <summary>
        /// Returns details of the contact   
        /// </summary>
        /// <returns></returns>
        [HttpGet("details", Name = ContactsControllerRoute.GetContactDetails)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(Contact))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetContactDetails()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetContactDetails(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// This method is called to update the detail of contact and returns the final situation of the contact details.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("updatedetails", Name = ContactsControllerRoute.UpdateContactDetails)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(Contact))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> UpdateContactDetails(ContactUpdateRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.UpdateContactDetails(request);
        }

        /// <summary>
        /// Returns the password policy
        /// </summary>
        /// <returns></returns>
        [HttpGet("passwordpolicy", Name = ContactsControllerRoute.GetPasswordPolicySettings)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(PasswordSettings))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetPasswordPolicySettings()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;
            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetPasswordPolicySettings();
        }

        /// <summary>
        /// This method is called to register a new contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("newregistration", Name = ContactsControllerRoute.AddNewContact)]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(RegisterResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> RegisterContact(RegisterContactRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.RegisterContact(request);
        }

        /// <summary>
        /// This method is called to confirm the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("contactconfirm", Name = ContactsControllerRoute.ConfirmContact)]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ConfirmResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> ConfirmContact(ConfirmContactRequest request)
        {
            return await _contactService.ConfirmContact(request);
        }
        
        /// <summary>
        /// Returns reservations of the contact with flight and accommodation informations.
        /// </summary>
        /// <returns></returns>
        [HttpGet("reservations", Name = ContactsControllerRoute.GetContactReservations)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(List<ContactReservationResponse>))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetContactReservations()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetContactReservations(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// Return travellers of the contact
        /// </summary>
        /// <returns></returns>
        [HttpGet("travellers", Name = ContactsControllerRoute.GetContactTravellers)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ContactsContactTraveller[]))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetContactTravellers()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetContactTravellers(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// Adds a new traveller for the contact
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("travellerinsert", Name = ContactsControllerRoute.AddTravellerToContact)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(AddTravellerToContactRequest))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> AddOrUpdateTraveller(AddTravellerToContactRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.AddOrUpdateTraveller(request);
        }

        /// <summary>
        /// Updates the contact's traveller
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("travellerupdate", Name = ContactsControllerRoute.UpdateContactTraveller)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(AddTravellerToContactRequest))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> UpdateContactTraveller(AddTravellerToContactRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.AddOrUpdateTraveller(request);
        }

        /// <summary>
        /// Deletes the contact's traveller by id
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("travellerdelete", Name = ContactsControllerRoute.DeleteContactTraveller)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(DeleteContactTravellerResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> DeleteContactTraveller(DeleteContactTravellerRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.DeleteContactTraveller(request);
        }

        /// <summary>
        /// Uploads an image file for profile picture of the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("uploadpicture", Name = ContactsControllerRoute.UploadProfilePicture)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(UploadResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> UploadProfilePicture(ProfilePictureUploadRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.UploadProfilePicture(request);
        }

        /// <summary>
        /// Deletes profile picture of the contact.
        /// </summary>
        /// <returns></returns>
        [HttpDelete("picturedelete", Name = ContactsControllerRoute.DeleteProfilePicture)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(DeleteProfilePictureResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> DeleteProfilePicture()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.DeleteProfilePicture(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// Changes password of the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("passwordchange", Name = ContactsControllerRoute.ChangePassword)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ChangePasswordResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> ChangePassword(ChangePasswordRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.ChangePassword(request);
        }

        /// <summary>
        /// Returns phone numbers of the contact
        /// </summary>
        /// <returns></returns>
        [HttpGet("telephonenumbers", Name = ContactsControllerRoute.GetTelephoneNumberList)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ContactTelephoneNumbersResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetTelephoneNumberList()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetTelephoneNumberList(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// Updates phone number(s) of the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("telephoneupdate", Name = ContactsControllerRoute.UpdateContactPhoneNumbers)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ContactTelephoneNumbersResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> UpdateContactPhoneNumbers(UpdateContactPhoneNumberRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.UpdateContactPhoneNumbers(request);
        }

        /// <summary>
        /// Returns address of the contact
        /// </summary>
        /// <returns></returns>
        [HttpGet("address", Name = ContactsControllerRoute.GetContactAddressList)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ContactsAccountAddress))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetContactAddressList()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetContactAddressList(Convert.ToInt32(contactId));
        }

        /// <summary>
        /// Updates address of the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("addressupdate", Name = ContactsControllerRoute.UpdateContactAddress)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ContactsAccountAddress))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> UpdateContactAddress(UpdateContactAddressRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.UpdateContactAddress(request);
        }

        /// <summary>
        /// Returns emails of the contact
        /// </summary>
        /// <returns></returns>
        [HttpGet("emails", Name = ContactsControllerRoute.GetContactEmailList)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(List<AccountEmailResponse>))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> GetContactEmailList()
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.GetContactEmailList(Convert.ToInt32(contactId));
        }

        /// <summary>
        ///  Adds a new email address to the contact
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("emailinsert", Name = ContactsControllerRoute.AddEmailToContact)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(AccountEmailResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> AddEmailToContact(AddEmailRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.AddEmailToContact(request);
        }

        /// <summary>
        /// Deletes an email address from the contact.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpDelete("emaildelete", Name = ContactsControllerRoute.DeleteEmail)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(DeleteEmailOfContactResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> DeleteEmailOfContact(DeleteEmailRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.DeleteEmailOfContact(request);
        }

        /// <summary>
        /// Sends an email to the added email address to confirm it.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("emailsendconfirmation", Name = ContactsControllerRoute.SendConfirmationEmail)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(SendConfirmationEmailResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> SendConfirmationEmail(SendConfirmationEmailRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.SendConfirmationEmail(request);
        }

        /// <summary>
        /// Confirms the added email address
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("emailconfirm", Name = ContactsControllerRoute.ConfirmEmail)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(ConfirmEmailResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> ConfirmEmail(EmailConfirmRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            return await _contactService.ConfirmEmail(request);
        }

        /// <summary>
        /// Changes one of the contact's email addresses as primary.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("emailmakeprimary", Name = ContactsControllerRoute.SetEmailAsPrimary)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(SetEmailAsPrimaryResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> SetEmailAsPrimary(SetPrimaryEmailRequest request)
        {
            var contactId = User.FindFirst(ClaimTypeContants.ContactId).Value;

            if (string.IsNullOrEmpty(contactId))
                return Unauthorized();
            request.ContactId = Convert.ToInt32(contactId);
            return await _contactService.SetEmailAsPrimary(request);
        }

        /// <summary>
        /// Sends an email to the contact for the password renew.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost("passwordrenewrequest", Name = ContactsControllerRoute.PasswordRenew)]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(PasswordRenewResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> PasswordRenew(PasswordRenewRequest request)
        {
            return await _contactService.PasswordRenew(request);
        }

        /// <summary>
        /// Completes the password renew process.
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("passwordrenewcomplete", Name = ContactsControllerRoute.CompletePasswordRenew)]
        [AllowAnonymous]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(CompletePasswordRenewResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> CompletePasswordRenew(CompletePasswordRenewRequest request)
        {
            return await _contactService.CompletePasswordRenew(request);
        }

    }
}
