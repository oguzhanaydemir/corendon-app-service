﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MobileAPI.Classes;
using MobileAPI.Constants.RouteContants;
using MobileAPI.Models;
using MobileAPI.RequestModels;
using MobileAPI.Services;
using Swashbuckle.AspNetCore.Annotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Controllers
{
    [Route("api/[controller]")]
    public class AuthController : Controller
    {
        private readonly IAuthService _authService;

        public AuthController(IAuthService authService)
        {
            _authService = authService;
        }

        /// <summary>
        /// This method is called from client to log in.
        /// </summary>
        /// <param name="request">The request contains email and password informations</param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("login", Name = AuthControllerRoute.Login)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(LoginResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> Login([FromServices]AppAuthOptions authOptions, [FromBody] LoginRequest request)
        {
            return await _authService.Login(authOptions,request);
        }

        /// <summary>
        /// This method is called from client to get new token with the refresh token.
        /// </summary>
        /// <param name="request">The request contains the refresh token </param>
        /// <returns></returns>
        [AllowAnonymous]
        [HttpPost("refresh-token", Name = AuthControllerRoute.RefreshToken)]
        [SwaggerResponse(StatusCodes.Status200OK, "", typeof(RefreshTokenResponse))]
        [SwaggerResponse(StatusCodes.Status400BadRequest, "", typeof(ErrorResponse))]
        public async Task<IActionResult> RefreshToken([FromBody] RefreshTokenRequest request)
        {
            return await _authService.RefreshToken(request);
        }

    }
}
