﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class DeleteEmailRequest : BaseRequest
    {
        public bool Confirmed { get; set; }
        public int ContactEmailID { get; set; }
    }
}
