﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class SendConfirmationEmailRequest
    {
        public int ContactEmailID { get; set; }
        public string Email { get; set; }
    }
}
