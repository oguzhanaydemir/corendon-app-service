﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class ContactUpdateRequest : BaseRequest
    {

        public string FirstName { get; set; }
        public string Surname { get; set; }
        public int GenderID { get; set; }
        public string BirthDate { get; set; }
        public string CompanyName { get; set; }

    }
}
