﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class UpdateContactPhoneNumberRequest : BaseRequest
    {
        public string TelephoneNumber { get; set; }
        public string ExtraNumber { get; set; }
        public string HomeNumber { get; set; }
    }
}
