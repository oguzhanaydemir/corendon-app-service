﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class ConfirmContactRequest
    {
        public string EmailAddress { get; set; }
        public int ConfirmationNumber { get; set; }
        public bool DirectLogin { get; set; }
    }
}
