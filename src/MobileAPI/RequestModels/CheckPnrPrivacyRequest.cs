﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class CheckPnrPrivacyRequest : BaseRequest
    {
        public string PNR { get; set; }
        public string Surname { get; set; }
        public string BirthDate { get; set; }
    }
}
