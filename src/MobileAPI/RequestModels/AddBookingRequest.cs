﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class AddBookingRequest : BaseRequest
    {
        /// <summary>
        /// It must be PNR
        /// </summary>
        public string ID { get; set; }
        /// <summary>
        /// It must be a surname of passengers in booking
        /// </summary>
        public string Surname { get; set; }
    }
}
