﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class DeleteContactTravellerRequest : BaseRequest
    {
        public int ContactTravellerID { get; set; }
    }
}
