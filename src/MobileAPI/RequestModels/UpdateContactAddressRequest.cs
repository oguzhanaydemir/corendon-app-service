﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class UpdateContactAddressRequest : BaseRequest
    {
        public int AddressTypeID { get; set; }
        public int ContactAddressID { get; set; }
        public string Country { get; set; }
        public string CountryCode { get; set; }
        public int? CountryID { get; set; }
        public int? Number { get; set; }
        public string NumberExtension { get; set; }
        public string Place { get; set; }
        public string PostalCode { get; set; }
        public string StreetName { get; set; }
        public string Type { get; set; }
    }
}
