﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class CompletePasswordRenewRequest
    {
        public string Email { get; set; }
        public string AuthKey { get; set; }
        public string Password { get; set; }
    }
}
