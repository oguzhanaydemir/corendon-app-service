﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class ProfilePictureUploadRequest : BaseRequest
    {
        public string FileName { get; set; }
        public byte[] ImageFileContent { get; set; }
    }
}
