﻿namespace MobileAPI.RequestModels
{
    public class CancelBookingRequest : BaseRequest
    {
        public string ID { get; set; }
        public string IBAN { get; set;}
        public string BIC { get; set;}
    }
}
