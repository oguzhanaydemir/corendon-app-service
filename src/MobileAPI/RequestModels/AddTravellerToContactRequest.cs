﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class AddTravellerToContactRequest : BaseRequest
    {
        public DateTime? BirthDate { get; set; }
        public int ContactTravellerID { get; set; }
        public string Email { get; set; }
        public int GenderID { get; set; }
        public string Name { get; set; }
        public int OrderNumber { get; set; }
        public string Surname { get; set; }
    }
}
