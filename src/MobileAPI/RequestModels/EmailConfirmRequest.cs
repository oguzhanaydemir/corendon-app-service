﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class EmailConfirmRequest
    {
        public int ContactEmailID { get; set; }
        public int ConfirmationNumber { get; set; }
    }
}
