﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.RequestModels
{
    public class PasswordRenewRequest
    {
        public string Email { get; set; }
    }
}
