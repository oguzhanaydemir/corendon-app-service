﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants
{
    public static class LanguageCodes
    {
        public const string NL = nameof(NL);
        public const string FR = nameof(FR);
    }
}
