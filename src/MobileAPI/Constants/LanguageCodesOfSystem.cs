﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants
{
    public static class LanguageCodesOfSystem
    {
        public const string NL = nameof(NL);
        public const string NL_BE = "NL-BE";
        public const string FR_BE = "FR-BE";
    }
}
