﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants
{
    public static class Brands
    {
        public const string MijnNL = "mijn.corendon.nl";
        public const string MijnBE = "mijn.corendon.be";
        public const string MonBE = "mon.corendon.be";

        public const string NLWebSiteUrl = "https://www.corendon.nl";
        public const string BEWebSiteUrl = "https://www.corendon.be";
        public const string BEFRWebSiteUrl = "https://fr.corendon.be";

        //mijn.corendon.nl'in language id'si
        public const int DefaultLanguageId = 2;
        public const string MobileAppKey = "ptksp67r5Kle";
    }
}
