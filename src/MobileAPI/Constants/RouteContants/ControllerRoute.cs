﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants.RouteContants
{
    public static class ControllerRoute
    {
        public const string Bookings = nameof(Bookings);
        public const string Contacts = nameof(Contacts);
        public const string Auth = nameof(Auth);
    }
}
