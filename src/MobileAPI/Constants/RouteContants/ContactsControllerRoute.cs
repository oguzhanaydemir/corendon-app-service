﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants.RouteContants
{
    public static class ContactsControllerRoute
    {
        public const string GetContactDetails = ControllerRoute.Contacts + "_" + nameof(GetContactDetails);
        public const string UpdateContactDetails = ControllerRoute.Contacts + "_" + nameof(UpdateContactDetails);
        public const string GetPasswordPolicySettings = ControllerRoute.Contacts + "_" + nameof(GetPasswordPolicySettings);
        public const string AddNewContact = ControllerRoute.Contacts + "_" + nameof(AddNewContact);
        public const string ConfirmContact = ControllerRoute.Contacts + "_" + nameof(ConfirmContact);
        public const string GetContactReservations = ControllerRoute.Contacts + "_" + nameof(GetContactReservations);
        public const string GetContactTravellers = ControllerRoute.Contacts + "_" + nameof(GetContactTravellers);
        public const string AddTravellerToContact = ControllerRoute.Contacts + "_" + nameof(AddTravellerToContact);
        public const string UpdateContactTraveller = ControllerRoute.Contacts + "_" + nameof(UpdateContactTraveller);
        public const string DeleteContactTraveller = ControllerRoute.Contacts + "_" + nameof(DeleteContactTraveller);
        public const string UploadProfilePicture = ControllerRoute.Contacts + "_" + nameof(UploadProfilePicture);
        public const string DeleteProfilePicture = ControllerRoute.Contacts + "_" + nameof(DeleteProfilePicture);
        public const string ChangePassword = ControllerRoute.Contacts + "_" + nameof(ChangePassword);
        public const string GetTelephoneNumberList = ControllerRoute.Contacts + "_" + nameof(GetTelephoneNumberList);
        public const string UpdateContactPhoneNumbers = ControllerRoute.Contacts + "_" + nameof(UpdateContactPhoneNumbers);
        public const string GetContactAddressList = ControllerRoute.Contacts + "_" + nameof(GetContactAddressList);
        public const string UpdateContactAddress = ControllerRoute.Contacts + "_" + nameof(UpdateContactAddress);
        public const string GetContactEmailList = ControllerRoute.Contacts + "_" + nameof(GetContactEmailList);
        public const string AddEmailToContact = ControllerRoute.Contacts + "_" + nameof(AddEmailToContact);
        public const string DeleteEmail = ControllerRoute.Contacts + "_" + nameof(DeleteEmail);
        public const string SendConfirmationEmail = ControllerRoute.Contacts + "_" + nameof(SendConfirmationEmail);
        public const string ConfirmEmail = ControllerRoute.Contacts + "_" + nameof(ConfirmEmail);
        public const string SetEmailAsPrimary = ControllerRoute.Contacts + "_" + nameof(SetEmailAsPrimary);
        public const string PasswordRenew = ControllerRoute.Contacts + "_" + nameof(PasswordRenew);
        public const string CompletePasswordRenew = ControllerRoute.Contacts + "_" + nameof(CompletePasswordRenew);
        
    }
}
