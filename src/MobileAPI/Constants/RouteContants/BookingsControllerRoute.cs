﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants.RouteContants
{
    public static class BookingsControllerRoute
    {
        public const string Detail = ControllerRoute.Bookings + "_" + nameof(Detail);
        public const string PaymentHistory = ControllerRoute.Bookings + "_" + nameof(PaymentHistory);
        public const string Add = ControllerRoute.Bookings + "_" + nameof(Add);
        public const string GetVoucher = ControllerRoute.Bookings + "_" + nameof(GetVoucher);
        public const string GetTicket = ControllerRoute.Bookings + "_" + nameof(GetTicket);
        public const string InvoiceSendEmail = ControllerRoute.Bookings + "_" + nameof(InvoiceSendEmail);
        public const string CancelBooking = ControllerRoute.Bookings + "_" + nameof(CancelBooking);
        public const string CheckPnrPrivacy = ControllerRoute.Bookings + "_" + nameof(CheckPnrPrivacy);
        public const string Review = ControllerRoute.Bookings + "_" + nameof(Review);

    }
}
