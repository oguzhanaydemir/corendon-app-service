﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Constants.RouteContants
{
    public class AuthControllerRoute
    {
        public const string Login = ControllerRoute.Auth + "_" + nameof(Login);
        public const string RefreshToken = ControllerRoute.Auth + "_" + nameof(RefreshToken);
    }
}
