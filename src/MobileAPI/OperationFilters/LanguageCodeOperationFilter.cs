﻿using Microsoft.OpenApi.Any;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace MobileAPI.OperationFilters
{
    public class LanguageCodeOperationFilter : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.Parameters == null)
                operation.Parameters = new List<OpenApiParameter>();

            operation.Parameters.Add
            (
                new OpenApiParameter 
                {
                    Description = "Used to uniquely identify the request language",
                    In = ParameterLocation.Header,
                    Name = "LanguageCode",
                    Required = false,
                    Schema = new OpenApiSchema {
                        Type = "string",
                        Default = new OpenApiString("nl-nl")
                    }
                }
            );
        }
    }
}
