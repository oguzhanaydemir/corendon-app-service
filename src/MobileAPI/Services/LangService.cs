﻿using Microsoft.Extensions.Caching.Memory;
using MobileAPI.Constants;
using PS.GeneralService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public class LangService : ILangService
    {
        private Hashtable _langKeywordsTable = new Hashtable();
        private readonly IBrandService _brandService;
        private readonly IGeneral _generalService;
        private readonly IMemoryCache _memoryCache;

        public LangService(IBrandService brandService, IGeneral generalService, IMemoryCache memoryCache)
        {
            _brandService = brandService;
            _generalService = generalService;
            _memoryCache = memoryCache;
        }

        public async Task<List<LangClassLanguage>> GetLanguages()
        {
            try
            {
                var cacheStr = "LanguageList";
                List<LangClassLanguage> languages;

                if (!_memoryCache.TryGetValue(cacheStr, out languages))
                {
                    languages = (await _generalService.LanguageListAsync(_brandService.Options.LoginName, _brandService.Options.Password)).ToList();
                    _memoryCache.Set(cacheStr, languages, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpiration = DateTime.Now.AddHours(2),
                        Priority = CacheItemPriority.Low
                    });
                }

                return languages;

            }
            catch (Exception)
            {

            }
            return null;


        }

        public string GetValueByKey(string key)
        {
            var value = _langKeywordsTable[key];

            if (value != null && !string.IsNullOrEmpty(Convert.ToString(value)))
                return value.ToString();

            return "##" + key + "##";
        }

        public async Task GetValueList(List<string> keys)
        {
            var languageId = await GetLanguageIdByCode(_brandService.LanguageCodeForResponseMessages) ?? Brands.DefaultLanguageId;
            _langKeywordsTable.Clear();
            var willGetFromDB = new List<string>();

            foreach (string key in keys)
            {
                var cacheStr = $"lngk_{languageId}_{key}";

                string value;

                if (_memoryCache.TryGetValue(cacheStr, out value))
                {
                    _langKeywordsTable.Add(key, value);
                }
                else
                {
                    willGetFromDB.Add(key);
                }
            }

            if (willGetFromDB.Count > 0)
            {
                var keyWordList = (await _generalService.LanguageKeywordsAsync(_brandService.Options.LoginName, _brandService.Options.Password, languageId, keys.ToArray())).ToList();

                foreach (var keyWordObj in keyWordList)
                {
                    _langKeywordsTable.Add(keyWordObj.KeyCode, keyWordObj.Keyword);

                    _memoryCache.Set($"lngk_{languageId}_{keyWordObj.KeyCode}", keyWordObj.Keyword, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpiration = DateTime.Now.AddHours(1),
                        Priority = CacheItemPriority.High
                    });
                }
            }
        }

        private async Task<int?> GetLanguageIdByCode(string languageCode)
        {
            try
            {
                var languages = await GetLanguages();

                if (languages.Any(l => l.Code.Trim() == languageCode.Trim()))
                    return languages.Where(l => l.Code.Trim() == languageCode.Trim()).Select(l => l.ID).FirstOrDefault();

                return Brands.DefaultLanguageId;
            }
            catch (Exception)
            {
            }

            return null;
        }
    }
}
