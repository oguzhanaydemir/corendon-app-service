﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.RequestModels;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public interface IContactManager
    {
        Task<IActionResult> GetContactDetails(int contactId);
        Task<IActionResult> UpdateContactDetails(ContactUpdateRequest request);
        Task<IActionResult> GetPasswordPolicySettings();
        Task<IActionResult> RegisterContact(RegisterContactRequest request);
        Task<IActionResult> ConfirmContact(ConfirmContactRequest request);
        Task<IActionResult> GetContactReservations(int contactId);
        Task<IActionResult> GetContactTravellers(int contactId);
        Task<IActionResult> AddOrUpdateTraveller(AddTravellerToContactRequest request);
        Task<IActionResult> DeleteContactTraveller(DeleteContactTravellerRequest request);
        Task<IActionResult> UploadProfilePicture(ProfilePictureUploadRequest request);
        Task<IActionResult> DeleteProfilePicture(int contactId) ;
        Task<IActionResult> ChangePassword(ChangePasswordRequest request);
        Task<IActionResult> GetTelephoneNumberList(int contactId) ;
        Task<IActionResult> UpdateContactPhoneNumbers(UpdateContactPhoneNumberRequest request);
        Task<IActionResult> GetContactAddressList(int contactId) ;
        Task<IActionResult> UpdateContactAddress(UpdateContactAddressRequest request);
        Task<IActionResult> GetContactEmailList(int contactId) ;
        Task<IActionResult> AddEmailToContact(AddEmailRequest request);
        Task<IActionResult> DeleteEmailOfContact(DeleteEmailRequest request);
        Task<IActionResult> SendConfirmationEmail(SendConfirmationEmailRequest request);
        Task<IActionResult> ConfirmEmail(EmailConfirmRequest request);
        Task<IActionResult> SetEmailAsPrimary(SetPrimaryEmailRequest request);
        Task<IActionResult> PasswordRenew(PasswordRenewRequest request);
        Task<IActionResult> CompletePasswordRenew(CompletePasswordRenewRequest request);

    }
}