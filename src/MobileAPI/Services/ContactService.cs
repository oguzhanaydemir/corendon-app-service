﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using MobileAPI.Classes;
using MobileAPI.Constants;
using MobileAPI.Extensions;
using MobileAPI.Helpers;
using MobileAPI.Models;
using MobileAPI.RequestModels;
using PS.BookingService;
using PS.ContactService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public class ContactService : IContactManager
    {
        private readonly IBrandService _brandService;
        private readonly ILangService _langService;
        private readonly IContactService _contactService;
        private readonly IBookingService _bookingService;
        private readonly IMemoryCache _memoryCache;
        private readonly ITokenGenerator _tokenGenerator;

        public ContactService(IBrandService brandService, ILangService langService, IContactService contactService, IBookingService bookingService, IMemoryCache memoryCache, ITokenGenerator tokenGenerator)
        {
            _brandService = brandService;
            _langService = langService;
            _contactService = contactService;
            _bookingService = bookingService;
            _memoryCache = memoryCache;
            _tokenGenerator = tokenGenerator;
        }

        public async Task<IActionResult> AddEmailToContact(AddEmailRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "AN_ERROR_OCCURED_WHILE_PROCESSING", "INTERNAL_SERVER_ERROR" });

                var presp = await _contactService.EmailInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, 2, request.Email, _brandService.BrandLanguage);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"),
                        DebugInfo = presp.Message
                    });
                }

                int contactPreConfirmEmailId = Convert.ToInt32(presp.IntParam1);

                var serviceResponse = await _contactService.EmailConfirmationSendAsync(_brandService.Options.LoginName, _brandService.Options.Password, null, contactPreConfirmEmailId, request.Email, _brandService.BrandLanguage, false, null, false, null, true);

                var response = new AccountEmailResponse
                {
                    Confirmed = false,
                    ContactEmailID = contactPreConfirmEmailId,
                    Email = request.Email,
                    EmailTypeID = 2,
                    TypeName = "Others"
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 2,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> AddOrUpdateTraveller(AddTravellerToContactRequest request)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING" });
            try
            {
                var processResponse = await _contactService.TravellerInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, request.ContactTravellerID, request.GenderID
                    , request.Name, request.Surname, request.BirthDate, request.OrderNumber, request.Email
                    , _brandService.BrandLanguage);
                if (processResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = processResponse.Message });
                }

                if (request.ContactTravellerID == 0)
                {
                    request.ContactTravellerID = Convert.ToInt32(processResponse.IntParam1);
                }

                return new OkObjectResult(request);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> ChangePassword(ChangePasswordRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "PASSWORD_POLICY_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "YOUR_PASSWORD_HAS_BEEN_CHANGED" });

                if (!await _contactService.PasswordCheckAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.NewPassword))
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("PASSWORD_POLICY_ERROR"),
                        DebugInfo = "New password is not valid"
                    });
                }

                var presp = await _contactService.Account_PasswordChangeAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, request.NewPassword, _brandService.BrandLanguage);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 7,
                        Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"),
                        DebugInfo = presp.Message
                    });
                }

                var response = new ChangePasswordResponse
                {
                    Message = _langService.GetValueByKey("YOUR_PASSWORD_HAS_BEEN_CHANGED")

                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 2,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> CompletePasswordRenew(CompletePasswordRenewRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INTERNAL_SERVER_ERROR", "INVALID_PARAMETER_VAL_EMAIL", "INVALID_PARAMETER_VAL_AUTH", "PASSWORD_RENEW_CHECK_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "YOUR_PASSWORD_HAS_BEEN_CHANGED" });

                if (string.IsNullOrEmpty(request.Email))
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 3,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_EMAIL"),
                        DebugInfo = "E-mail is empty"
                    });
                }
                if (string.IsNullOrEmpty(request.AuthKey))
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 4,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_AUTH")
                        ,
                        DebugInfo = "AuthKey is empty"
                    });
                }



                var presp = await _contactService.AccountPasswordResetCheckStatusAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.Email, request.AuthKey, _brandService.BrandLanguage);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 5,
                        Message = _langService.GetValueByKey("PASSWORD_RENEW_CHECK_ERROR"),
                        DebugInfo = presp.Message
                    });
                }

                presp = await _contactService.AccountCompletePasswordResetAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.Email, request.AuthKey, request.Password, _brandService.BrandLanguage);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"),
                        DebugInfo = presp.Message
                    });
                }

                //var contactId = Convert.ToInt32(presp.IntParam1);

                var response = new CompletePasswordRenewResponse
                {
                    Message = _langService.GetValueByKey("YOUR_PASSWORD_HAS_BEEN_CHANGED")
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> ConfirmContact(ConfirmContactRequest request)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_EMAIL", "INVALID_PARAMETER_VAL_CONFIRMNUMBER", "EMAIL_NOTFOUND_INPREREGISTRATION", "CONFIRM_ERROR_GET_CONTACT" });

            if (string.IsNullOrEmpty(request.EmailAddress))
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 3, Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_EMAIL"), DebugInfo = "E-mail Address is empty" });
            }

            if (request.ConfirmationNumber == 0)
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 4,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_CONFIRMNUMBER"),
                        DebugInfo = "ConfirmationNumber is 0"
                    }
                );
            }

            var presp = await _contactService.ContactPreRegistrationIDGetAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.EmailAddress);

            if (presp.HasError)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 5,
                    Message = _langService.GetValueByKey("EMAIL_NOTFOUND_INPREREGISTRATION"),
                    DebugInfo = presp.Message
                });
            }

            var contactPreRegistrationID = Convert.ToInt32(presp.IntParam1);

            presp = await _contactService.ConfirmationAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactPreRegistrationID, request.ConfirmationNumber);
            if (presp.HasError)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 6,
                    Message = _langService.GetValueByKey("EMAIL_NOTFOUND_INPREREGISTRATION"),
                    DebugInfo = presp.Message
                });
            }
            else
            {
                await _contactService.AfterConfirmationSendEmailAsync(_brandService.Options.LoginName, _brandService.Options.Password, Convert.ToInt32(presp.IntParam1), _brandService.BrandLanguage, false, null, false, null);

            }

            if (!request.DirectLogin)
            {
                var response = new ConfirmResponse
                {
                    ContactID = Convert.ToInt32(presp.IntParam1)
                };

                return new OkObjectResult(response);
            }

            try
            {
                var response = new Models.LoginResponse
                {
                    Token = _tokenGenerator.CreateToken(Convert.ToString(Convert.ToInt32(presp.IntParam1))),
                    RefreshToken = EncryptionHelper.Encrypt(presp.StringParam1),
                    ContactInfo = await _contactService.AccountDetailsAsync(_brandService.Options.LoginName, _brandService.Options.Password, Convert.ToInt32(presp.IntParam1), _brandService.BrandLanguage)
                };

                return new OkObjectResult(response);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 7,
                    Message = _langService.GetValueByKey("CONFIRM_ERROR_GET_CONTACT"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> ConfirmEmail(EmailConfirmRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "EMAIL_IS_CONFIRMED" });

                var presp = await _contactService.EmailConfirmationAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactEmailID, request.ConfirmationNumber);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = presp.Message });
                }

                return new OkObjectResult(new ConfirmEmailResponse { Message = _langService.GetValueByKey("EMAIL_IS_CONFIRMED") + " E-mail=" + presp.StringParam1 });
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> DeleteContactTraveller(DeleteContactTravellerRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "CONTACT_TRAVELLER_DELETED" });

                var presp = await _contactService.TravellerDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactTravellerID, request.ContactId, _brandService.BrandLanguage);
                
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = presp.Message });
                }

                var response = new DeleteContactTravellerResponse
                {
                    Message = _langService.GetValueByKey("CONTACT_TRAVELLER_DELETED")
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> DeleteEmailOfContact(DeleteEmailRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "EMAIL_DOESNT_EXISTS", "EMAIL_IS_DELETED" });

                var processResponse = new PS.ContactService.ProcessResponse();

                if (request.Confirmed)
                {
                    processResponse = await _contactService.EmailDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactEmailID, request.ContactId, _brandService.BrandLanguage);
                }
                else
                {
                    processResponse = await _contactService.PreConfirmEmailDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactEmailID, request.ContactId, _brandService.BrandLanguage);
                }

                if (processResponse.HasError && processResponse.ErrorCode == -1)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 7, Message = _langService.GetValueByKey("EMAIL_DOESNT_EXISTS"), DebugInfo = processResponse.Message });
                }

                if (processResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = processResponse.Message });
                }

                return new OkObjectResult(new DeleteEmailOfContactResponse { Message = _langService.GetValueByKey("EMAIL_IS_DELETED") });
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> DeleteProfilePicture(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "PROFILE_PICTURE_DELETED_MSG" });

                var presp = await _contactService.PictureDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

                if (presp.HasError)

                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = presp.Message });

                var response = new DeleteProfilePictureResponse
                {
                    Message = _langService.GetValueByKey("PROFILE_PICTURE_DELETED_MSG")
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 2,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> GetContactAddressList(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

                var addressList = await _contactService.AddressesAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

                var response = new ContactsAccountAddress();

                if (addressList.Length > 0)
                {
                    response = addressList[0];
                }

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> GetContactDetails(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });
                var response = await _contactService.AccountDetailsAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);
                return new OkObjectResult(response);
            }

            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> GetContactEmailList(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING" });

                var emails = await _contactService.EmailsAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

                var response = new List<AccountEmailResponse>();
                if (emails != null && emails.Length > 0)
                    response = emails?.Select(e => new AccountEmailResponse
                    {
                        Confirmed = e.Confirmed,
                        ContactEmailID = e.Confirmed ? e.ContactEmailID : e.ContactPreConfirmEmailID,
                        Email = e.Email,
                        EmailTypeID = e.EmailTypeID,
                        TypeName = e.TypeName
                    }).ToList();


                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> GetContactReservations(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

                var response = await BindReservationsAsync(contactId);
                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> GetContactTravellers(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

                var response = await _contactService.ContactTravellersAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> GetPasswordPolicySettings()
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INTERNAL_SERVER_ERROR" });
                var cacheStr = "PasswordPolicySettings";
                PasswordSettings pwdSettings;

                if (!_memoryCache.TryGetValue(cacheStr, out pwdSettings))
                {
                    pwdSettings = await _contactService.PasswordSettingsAsync(_brandService.Options.LoginName, _brandService.Options.Password);
                    _memoryCache.Set(cacheStr, pwdSettings, new MemoryCacheEntryOptions
                    {
                        AbsoluteExpiration = DateTime.Now.AddMinutes(7),
                        Priority = CacheItemPriority.Low
                    });
                }

                return new OkObjectResult(pwdSettings);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 1,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }
        }

        public async Task<IActionResult> GetTelephoneNumberList(int contactId)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

                var phoneNumbers = await _contactService.PhoneNumbersAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

                var response = new ContactTelephoneNumbersResponse();
                foreach (var pnumb in phoneNumbers)
                {
                    if (pnumb.Type == "Primary")
                    {
                        response.TelephoneNumber = pnumb.FullNumber;
                    }
                    else if (pnumb.PhoneNumberTypeID == 3)
                    {
                        response.ExtraNumber = pnumb.FullNumber;
                    }
                    else if (pnumb.PhoneNumberTypeID == 5)
                    {
                        response.HomeNumber = pnumb.FullNumber;
                    }
                }

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }
        public async Task<IActionResult> PasswordRenew(PasswordRenewRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "INTERNAL_SERVER_ERROR", "PASS_RESET_ACCOUNT_CHECK_ERR", "ERROR_WHILE_EMAIL_SEND", "EMAIL_IS_SENT" });

                if (string.IsNullOrEmpty(request.Email))
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 3, Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_AUTH"), DebugInfo = "E-mail is empty" });
                }

                var presp = await _contactService.PasswordResetRequestAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.Email, _brandService.BrandLanguage, string.Empty, true);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 4,
                        Message = _langService.GetValueByKey("PASS_RESET_ACCOUNT_CHECK_ERR"),
                        DebugInfo = presp.Message
                    });
                }
                string AuthKey = presp.StringParam1;

                presp = await _contactService.PasswordRenewSendEmailAsync(_brandService.Options.LoginName, _brandService.Options.Password, "", _brandService.BrandLanguage, request.Email, AuthKey, false, null, false, null, true);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 5,
                        Message = _langService.GetValueByKey("ERROR_WHILE_EMAIL_SEND"),
                        DebugInfo = presp.Message
                    });
                }

                var response = new PasswordRenewResponse
                {
                    Message = _langService.GetValueByKey("EMAIL_IS_SENT")
                };

                return new OkObjectResult(response);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> RegisterContact(RegisterContactRequest request)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "INVALID_PARAMETER_VAL_EMAIL", "INVALID_NAME_PARAMETER_VAL", "INVALID_SURNAME_PARAMETER_VAL", "INVALID_GENDERID_PARAMETER_VAL", "EMAIL_ALREADY_EXISTS", "COULDNT_BE_REGISTER", "NEW_REGISTRATION_BUT_PROCESS_ERR" });

            if (string.IsNullOrEmpty(request.EmailAddress))
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 3, Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_EMAIL"), DebugInfo = "E-mail Address is empty" });
            }

            if (string.IsNullOrEmpty(request.FirstName))
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 4,
                    Message = _langService.GetValueByKey("INVALID_NAME_PARAMETER_VAL"),// "Invalid parameter value FirstName.",
                    DebugInfo = "FirstName is empty"
                });
            }
            if (string.IsNullOrEmpty(request.Surname))
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 5,
                    Message = _langService.GetValueByKey("INVALID_SURNAME_PARAMETER_VAL"),
                    DebugInfo = "Surname is empty"
                });
            }
            if (request.GenderID == 0)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 6,
                    Message = _langService.GetValueByKey("INVALID_GENDERID_PARAMETER_VAL"),
                    DebugInfo = "GenderID is 0"
                });
            }

            var presp = new PS.ContactService.ProcessResponse();
            presp = await _contactService.AccountNewRegistrationAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.EmailAddress, request.Password, request.FirstName
                   , request.Surname, request.GenderID, true, _brandService.BrandLanguage);

            if (presp.HasError)
            {
                if (presp.ErrorCode == -1)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 8,
                        Message = _langService.GetValueByKey("EMAIL_ALREADY_EXISTS"),
                        DebugInfo = presp.Message
                    });
                }
                else if (presp.ErrorCode == 4)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 12,
                        Message = _langService.GetValueByKey("PASSWORD_POLICY_ERROR"),
                        DebugInfo = presp.Message
                    });

                }

                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 7,
                    Message = _langService.GetValueByKey("COULDNT_BE_REGISTER"),
                    DebugInfo = presp.Message
                });

            }

            var contactPreRegistrationID = Convert.ToInt32(presp.IntParam1);
            var confirmationNumber = Convert.ToInt32(presp.IntParam2);


            var confirmSendResponse = await _contactService.AccountNewConfirmEmailSendAsync(_brandService.Options.LoginName, _brandService.Options.Password, string.Empty, _brandService.BrandLanguage, request.EmailAddress, request.FirstName, request.Surname, contactPreRegistrationID, confirmationNumber, false, null, false, null, true);

            if (confirmSendResponse.HasError)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 11,
                    Message = _langService.GetValueByKey("NEW_REGISTRATION_BUT_PROCESS_ERR"),
                    DebugInfo = presp.Message
                });
            }

            var response = new RegisterResponse
            {
                EmailAddress = request.EmailAddress
            };

            return new OkObjectResult(response);
        }

        public async Task<IActionResult> SendConfirmationEmail(SendConfirmationEmailRequest request)
        {
            try
            {

                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "CONFIRMATION_SENTMSG" });

                var presp = await _contactService.EmailConfirmationSendAsync(_brandService.Options.LoginName, _brandService.Options.Password, string.Empty, request.ContactEmailID, request.Email, _brandService.BrandLanguage, false, null, false, null, true);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = presp.Message });
                }

                var response = new SendConfirmationEmailResponse
                {
                    Message = _langService.GetValueByKey("CONFIRMATION_SENTMSG") + " E-mail=" + request.Email
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> SetEmailAsPrimary(SetPrimaryEmailRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING", "PRIMARY_EMAIL_IS_SETTED" });

                var presp = await _contactService.MakeEmailPrimaryAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactEmailID, request.ContactId, _brandService.BrandLanguage);
                if (presp.HasError)
                {
                    if (presp.ErrorCode != -2)
                    {
                        return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING"), DebugInfo = presp.Message });
                    }
                }
                var response = new SetEmailAsPrimaryResponse
                {
                    Message = _langService.GetValueByKey("PRIMARY_EMAIL_IS_SETTED")
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> UpdateContactAddress(UpdateContactAddressRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

                var contactsAccountAddresses = await _contactService.AddressesAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage);

                request.AddressTypeID = 1;

                if (contactsAccountAddresses.Length > 0) //these don't change from client
                {
                    request.ContactAddressID = contactsAccountAddresses[0].ContactAddressID;
                }

                if (!request.Number.HasValue)
                    request.Number = 0;

                if (!request.CountryID.HasValue)
                    request.CountryID = 154;

                var processResponse = new PS.ContactService.ProcessResponse();
                if (request.ContactAddressID == 0)
                {
                    processResponse = await _contactService.AddressInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, request.AddressTypeID, request.StreetName, request.Number.Value, request.NumberExtension, request.PostalCode, request.Place, request.CountryID.Value, _brandService.BrandLanguage);
                }
                else
                {
                    processResponse = await _contactService.AddressUpdateAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactAddressID, request.ContactId, request.AddressTypeID, request.StreetName, request.Number.Value, request.NumberExtension, request.PostalCode, request.Place, request.CountryID.Value, _brandService.BrandLanguage);
                }

                contactsAccountAddresses = await _contactService.AddressesAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage);

                var response = new ContactsAccountAddress();
                if (contactsAccountAddresses.Length > 0)
                {
                    response = contactsAccountAddresses[0];
                }
                return new OkObjectResult(response);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> UpdateContactDetails(ContactUpdateRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "RETURN_CONTACT_ISNULL", "RETURN_CONTACT_EMAIL_ISNULL", "INVALID_NAME_PARAMETER_VAL", "INVALID_SURNAME_PARAMETER_VAL", "INVALID_GENDERID_PARAMETER_VAL", "INPUT_PARAMETERS_EMPTY", "INVALID_BIRTHDATE", "CONTACT_COULDNT_BE_UPDATED" });

                var contactUpdateParams = new PS.ContactService.ContactUpdateParams
                {
                    FirstName = request.FirstName,
                    Surname = request.Surname,
                    BirthDate = request.BirthDate,
                    GenderID = request.GenderID,
                    CompanyName = request.CompanyName
                };

                var processResponse = await _contactService.Account_UpdateAccountDetailAllAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage, contactUpdateParams);

                if (processResponse.HasError)
                {
                    if (processResponse.ErrorCode == 1)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 6,
                            Message = _langService.GetValueByKey("INVALID_NAME_PARAMETER_VAL"),
                            DebugInfo = processResponse.Message
                        });

                    }

                    if (processResponse.ErrorCode == 2)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 7,
                            Message = _langService.GetValueByKey("INVALID_SURNAME_PARAMETER_VAL"),
                            DebugInfo = processResponse.Message
                        });
                    }

                    if (processResponse.ErrorCode == 3)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 8,
                            Message = _langService.GetValueByKey("INVALID_GENDERID_PARAMETER_VAL"),
                            DebugInfo = processResponse.Message
                        });
                    }

                    if (processResponse.ErrorCode == 5)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 9,
                            Message = _langService.GetValueByKey("INPUT_PARAMETERS_EMPTY"),
                            DebugInfo = processResponse.Message
                        });
                    }

                    if (processResponse.ErrorCode == 6)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 10,
                            Message = _langService.GetValueByKey("INVALID_BIRTHDATE"),
                            DebugInfo = processResponse.Message
                        });
                    }

                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 11,
                        Message = _langService.GetValueByKey("CONTACT_COULDNT_BE_UPDATED"),
                        DebugInfo = processResponse.Message
                    });
                }

                var response = await _contactService.AccountDetailsAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage);

                if (response == null)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 50,
                        Message = _langService.GetValueByKey("RETURN_CONTACT_ISNULL"),
                        DebugInfo = "return contact class is null"
                    });
                }

                if (response.Email == null)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 51,
                        Message = _langService.GetValueByKey("RETURN_CONTACT_EMAIL_ISNULL"),
                        DebugInfo = "return contact class email parameter is null"
                    });
                }

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 2,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> UpdateContactPhoneNumbers(UpdateContactPhoneNumberRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "TEL_ERROR_SAVE_PRIMARY", "TEL_ERROR_SAVE_EXTRA", "TEL_ERROR_SAVE_HOME" });

                var phoneNumbers = await _contactService.PhoneNumbersAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage);

                // TODO: alttaki üç değişkeni bi sınıfa taşı ve yeni bi metod ile değerlerini set et
                var primaryNumberId = default(int);
                var extraNumberID = default(int);
                var homeNumberId = default(int);

                foreach (var accountPhoneNumber in phoneNumbers)
                {
                    if (accountPhoneNumber.Type == "Primary")
                    {
                        primaryNumberId = accountPhoneNumber.ContactPhoneNumberID;
                    }
                    else if (accountPhoneNumber.PhoneNumberTypeID == 3)
                    {
                        extraNumberID = accountPhoneNumber.ContactPhoneNumberID;
                    }
                    else if (accountPhoneNumber.PhoneNumberTypeID == 5)
                    {
                        homeNumberId = accountPhoneNumber.ContactPhoneNumberID;
                    }
                }

                var processResponse = primaryNumberId == default ?
                    await _contactService.PhoneNumberInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, 1, request.TelephoneNumber, _brandService.BrandLanguage)
                    : await _contactService.PhoneNumberUpdateAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, primaryNumberId, 1, request.TelephoneNumber, _brandService.BrandLanguage);

                if (processResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("TEL_ERROR_SAVE_PRIMARY"),// "Error while Primary Phone number saving."
                        DebugInfo = processResponse.Message
                    });
                }

                if (string.IsNullOrEmpty(request.ExtraNumber) && extraNumberID != default)
                {
                    var deleteResponse = await _contactService.PhoneNumberDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, extraNumberID, request.ContactId, _brandService.BrandLanguage);
                    if (deleteResponse.HasError)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 7,
                            Message = _langService.GetValueByKey("TEL_ERROR_SAVE_EXTRA"), // "Error while Extra Phone number saving."
                            DebugInfo = deleteResponse.Message
                        });
                    }
                }
                else
                {
                    if (extraNumberID == default)
                    {
                        var addResponse = await _contactService.PhoneNumberInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, 3, request.ExtraNumber, _brandService.BrandLanguage);
                        if (addResponse.HasError)
                        {
                            return new BadRequestObjectResult(new ErrorResponse
                            {
                                ErrorCode = 7,
                                Message = _langService.GetValueByKey("TEL_ERROR_SAVE_EXTRA")//"Error while Extra phone number saving."
                                ,
                                DebugInfo = addResponse.Message
                            });
                        }
                    }
                    else
                    {
                        var updateResponse = await _contactService.PhoneNumberUpdateAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, extraNumberID, 3, request.ExtraNumber, _brandService.BrandLanguage);

                        if (updateResponse.HasError)
                        {
                            return new BadRequestObjectResult(new ErrorResponse
                            {
                                ErrorCode = 7,
                                Message = _langService.GetValueByKey("TEL_ERROR_SAVE_EXTRA"),//"Error while Extra phone number saving."
                                DebugInfo = updateResponse.Message
                            });
                        }
                    }
                }

                if (string.IsNullOrEmpty(request.HomeNumber))
                {
                    if (homeNumberId != 0)
                    {
                        var deleteResponse = await _contactService.PhoneNumberDeleteAsync(_brandService.Options.LoginName, _brandService.Options.Password, homeNumberId, request.ContactId, _brandService.BrandLanguage);
                        if (deleteResponse.HasError)
                        {
                            return new BadRequestObjectResult(new ErrorResponse
                            {
                                ErrorCode = 8,
                                Message = _langService.GetValueByKey("TEL_ERROR_SAVE_HOME"),// "Error while Home phone number saving."
                                DebugInfo = deleteResponse.Message
                            });
                        }
                    }
                }
                else
                {
                    var addOrUpdateResponse = homeNumberId == 0 ?
                        await _contactService.PhoneNumberInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, 5, request.HomeNumber, _brandService.BrandLanguage)
                        : await _contactService.PhoneNumberUpdateAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, homeNumberId, 5, request.HomeNumber, _brandService.BrandLanguage);

                    if (addOrUpdateResponse.HasError)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 8,
                            Message = _langService.GetValueByKey("TEL_ERROR_SAVE_HOME"),//"Error while Home phone number saving."
                            DebugInfo = addOrUpdateResponse.Message
                        });
                    }
                }

                var response = await GetTelephoneNumbersAsync(request.ContactId);

                return new OkObjectResult(response);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse
                {
                    ErrorCode = 2,
                    Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                    DebugInfo = err.ToString()
                });
            }

        }

        public async Task<IActionResult> UploadProfilePicture(ProfilePictureUploadRequest request)
        {
            try
            {
                await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "AN_ERROR_OCCURED_WHILE_PROCESSING" });

                var processResponse = await _contactService.UploadPictureAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, _brandService.BrandLanguage, request.FileName, request.ImageFileContent);

                if (processResponse.HasError)
                {
                    if (processResponse.ErrorCode == 1)
                    {
                        return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = processResponse.Message, DebugInfo = processResponse.Message });
                    }
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 7,
                        Message = _langService.GetValueByKey("AN_ERROR_OCCURED_WHILE_PROCESSING")// "An error occured while picture was processing."
                        ,
                        DebugInfo = processResponse.Message
                    });
                }

                var response = new UploadResponse
                {
                    ProfilePicture = processResponse.StringParam1,
                    ProfilePictureFullPath = processResponse.StringParam2
                };

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        private async Task<ContactTelephoneNumbersResponse> GetTelephoneNumbersAsync(int contactId)
        {

            var phonenumbers = await _contactService.PhoneNumbersAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage);

            var returnObj = new ContactTelephoneNumbersResponse();

            foreach (var pnumb in phonenumbers)
            {
                if (pnumb.Type == "Primary")
                {
                    returnObj.TelephoneNumber = pnumb.FullNumber;
                }
                else if (pnumb.PhoneNumberTypeID == 3)
                {
                    returnObj.ExtraNumber = pnumb.FullNumber;
                }
                else if (pnumb.PhoneNumberTypeID == 5)
                {
                    returnObj.HomeNumber = pnumb.FullNumber;
                }
            }
            return returnObj;
        }

        private async Task<List<ContactReservationResponse>> BindReservationsAsync(int contactId)
        {
            var reservations = await _bookingService.BookingListAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, _brandService.BrandLanguage, 999);

            if (reservations == null || reservations.Length == 0)
                return null;

            var returnReservationList = new List<ContactReservationResponse>();

            foreach (var reservation in reservations)
            {
                var willBeAddedReservation = new ContactReservationResponse
                {
                    AgentID = reservation.AgentNr,
                    AgentName = reservation.AgentName,
                    BookingDate = reservation.BookingDate,
                    ID = reservation.PNR,
                    StatusID = reservation.StatusID,
                    StatusName = reservation.StatusName,
                    CheckinDate = reservation.CheckinDate,
                    CheckoutDate = reservation.CheckoutDate,
                    TravelFrom = string.Empty,
                    TravelTill = string.Empty,
                    FirstPassengerSurname = reservation.FirstPassengerSurname,
                    Amount = !string.IsNullOrEmpty(reservation.CustomerNumber) ? reservation.Amount : default,
                    AmountOpen = !string.IsNullOrEmpty(reservation.CustomerNumber) ? reservation.AmountOpen : default,
                    Deposit = reservation.Deposit,
                    UserID = reservation.BookedFrom,
                    UserName = reservation.UserName,
                    Veranstalter = reservation.Veranstalter,
                    IsAgentBooking = reservation.IsAgentBooking,
                    HasPrivacyCheck = reservation.HasPrivacyCheck
                };

                willBeAddedReservation.Type = GetTypeOfReservation(reservation);

                if (willBeAddedReservation.Type == 1 || willBeAddedReservation.Type == 3)
                {
                    willBeAddedReservation.TravelFrom = reservation.Flights[0].FromAirportName;
                    willBeAddedReservation.TravelTill = reservation.Flights[0].ToAirportName;
                }

                if (willBeAddedReservation.Type == 2)
                {
                    willBeAddedReservation.TravelTill = reservation.AccommodationLocation;
                }


                willBeAddedReservation.Accommodations = FillAccommodationDetails(reservation.Rooms);
                willBeAddedReservation.Flights = FillReservationFlightDetails(reservation.Flights);

                returnReservationList.Add(willBeAddedReservation);
            }

            return returnReservationList;
        }

        private List<ContactReservationFlight> FillReservationFlightDetails(BookingListBookingRowFlight[] flights)
        {
            var retFlights = new List<ContactReservationFlight>();

            if (flights != null && flights.Count() > 0)
                retFlights = flights.Select(f =>
                                                new ContactReservationFlight
                                                {
                                                    AirlineID = f.AirlineCode,
                                                    AirlineName = f.AirlineName,
                                                    DepartureDate = f.DepartureDate.ToISOString(true),
                                                    ArrivalDate = f.ArrivalDate.ToISOString(true),
                                                    FlightNumber = f.FlightNumber,
                                                    FromAirportName = f.FromAirportName,
                                                    ToAirportName = f.ToAirportName,
                                                    ToAirportCode = f.ToAirportCode,
                                                    FromAirportCode = f.FromAirportCode,
                                                })
                                            .ToList();
            return retFlights;
        }

        private List<ContactReservationAccommodation> FillAccommodationDetails(BookingListBookingRowRoom[] rooms)
        {
            var accommodations = new List<ContactReservationAccommodation>();
            foreach (var roomItem in rooms)
            {
                if (!accommodations.Any(a => a.AccommodationID == roomItem.AccommodationCode))
                {
                    var accommodation = new ContactReservationAccommodation
                    {
                        AccommodationID = roomItem.AccommodationCode,
                        AccommodationName = roomItem.AccommodationName,
                        AccommodationTopPicture = roomItem.AccommodationTopPicture,
                        AccommodationTopPicture_Thumb = roomItem.AccommodationTopPicture_Thumb,
                        AccommodationStarRating = roomItem.AccommodationStarRating,
                        AccommodationURL = roomItem.AccommodationURL,
                    };

                    accommodation.Rooms = rooms
                                         .Where(r => r.AccommodationCode == roomItem.AccommodationCode)
                                         .Select(r => new ContactReservationRoom
                                         {
                                             BoardingID = r.BoardingType,
                                             BoardingName = r.BoardingName,
                                             CheckinDate = r.CheckinDate,
                                             CheckoutDate = r.CheckinDate.AddDays(r.Duration),
                                             RoomID = r.RoomType,
                                             RoomName = r.RoomName,
                                         })
                                         .ToList();
                    accommodations.Add(accommodation);
                }
            }
            return accommodations;
        }

        private int GetTypeOfReservation(BookingListBookingRow reservation)
        {
            // 1: Package 2: HotelOnly 3: FlightOnly 

            if (reservation.Flights.Length > 0 && reservation.Rooms.Length > 0)
                return 1;

            if (reservation.Rooms.Length > 0)
                return 2;

            if (reservation.Flights.Length > 0)
                return 3;

            return default;
        }
    }
}