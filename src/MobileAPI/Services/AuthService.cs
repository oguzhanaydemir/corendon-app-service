﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.Classes;
using MobileAPI.Helpers;
using MobileAPI.Models;
using MobileAPI.RequestModels;
using PS.ContactService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly ITokenGenerator _tokenGenerator;
        private readonly IBrandService _brandService;
        private readonly ILangService _langService;
        private readonly IContactService _contactService;

        public AuthService(ITokenGenerator tokenGenerator, IBrandService brandService, ILangService langService, IContactService contactService)
        {
            _tokenGenerator = tokenGenerator;
            _brandService = brandService;
            _langService = langService;
            _contactService = contactService;
            //_authOptions = authOptions;
        }
        public async Task<IActionResult> Login(AppAuthOptions authOptions, LoginRequest request)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_EMAIL", "INVALID_PARAMETER_VAL_PASSWORD", "LOGIN_ERROR" });

            if (string.IsNullOrEmpty(request.Email))
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 3,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_EMAIL"),
                        DebugInfo = "E-mail is empty"
                    }
                );
            }

            if (string.IsNullOrEmpty(request.Password))
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 4,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_PASSWORD"),
                        DebugInfo = "Password is empty"
                    }
                );
            }

            PS.ContactService.LoginResponse loginResponse;

            try
            {
                loginResponse = await _contactService.LoginWithEmailAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.Email, request.Password, !string.IsNullOrEmpty(_brandService.BrandLanguage) ? _brandService.BrandLanguage : "nl");
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INVALID_PARAMETER_VAL_PASSWORD"),
                        DebugInfo = err.ToString()
                    }
                );
            }

            if (loginResponse.HasError)
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 5,
                        Message = _langService.GetValueByKey("LOGIN_ERROR"),
                        DebugInfo = loginResponse.Message
                    }
                );
            }

            var sessionId = (await _contactService.SessionCreateAsync(_brandService.Options.LoginName, _brandService.Options.Password, true, authOptions.Key, loginResponse.ContactInfo.ContactID)).StringParam1;

            var hashedSessionId = EncryptionHelper.Encrypt(sessionId);

            var response = new Models.LoginResponse
            {
                Token = _tokenGenerator.CreateToken(Convert.ToString(loginResponse.ContactInfo.ContactID)),
                RefreshToken = hashedSessionId,
                ContactInfo = loginResponse.ContactInfo
            };

            return new OkObjectResult(response);
        }

        public async Task<IActionResult> RefreshToken(RefreshTokenRequest request)
        {
            try
            {
                var response = await _contactService.SessionRefreshTokenAsync(_brandService.Options.LoginName, _brandService.Options.Password, EncryptionHelper.Decrypt(request.RefreshToken));

                if (response.HasError)
                    return null;

                var token = _tokenGenerator.CreateToken(Convert.ToString(response.ContactID));

                return new OkObjectResult(new Models.RefreshTokenResponse { Token = token });
            }
            catch (Exception)
            {

                throw;
            }

        }
    }
}
