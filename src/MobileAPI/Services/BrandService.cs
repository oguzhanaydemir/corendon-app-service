﻿using MobileAPI.Classes;

namespace MobileAPI.Services
{
    public class BrandService : IBrandService
    {
        public BrandOptions Options { get; set; }
        public string Brand { get; set; }
        public string LanguageCodeForResponseMessages { get; set; }
        public string BrandLanguage { get; set; }
    }

}
