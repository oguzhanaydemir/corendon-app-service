﻿using PS.GeneralService;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public interface ILangService
    {
        Task GetValueList(List<string> keys);
        string GetValueByKey(string key);
        Task<List<LangClassLanguage>> GetLanguages();
    }
}
