﻿using MobileAPI.Classes;

namespace MobileAPI.Services
{
    public interface IBrandService
    {
        public BrandOptions Options { get; set; }
        public string Brand { get; set; }
        public string LanguageCodeForResponseMessages { get; set; }
        public string BrandLanguage { get; set; }
    }
}
