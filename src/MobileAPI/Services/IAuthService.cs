﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.Classes;
using MobileAPI.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public interface IAuthService
    {
        Task<IActionResult> Login(AppAuthOptions authOptions, LoginRequest request);
        Task<IActionResult> RefreshToken(RefreshTokenRequest request);
    }
}
