﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.Classes;
using MobileAPI.Extensions;
using MobileAPI.Models;
using MobileAPI.Models.ExternalAPIModels;
using MobileAPI.RequestModels;
using PS.BookingService;
using PS.ContactService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public class BookingService : IBookingManager
    {
        private readonly IBrandService _brandService;
        private readonly IBookingService _bookingClient;
        private readonly IContactService _contactClient;
        private readonly ILangService _langService;
        private readonly ReviewAPIOptions _reviewAPIOptions;

        public BookingService(IBrandService brandService, IBookingService bookingClient, IContactService contactClient, ILangService langService, ReviewAPIOptions reviewAPIOptions)
        {
            _brandService = brandService ?? throw new ArgumentNullException(nameof(brandService));
            _bookingClient = bookingClient ?? throw new ArgumentNullException(nameof(bookingClient));
            _contactClient = contactClient ?? throw new ArgumentNullException(nameof(contactClient));
            _langService = langService ?? throw new ArgumentNullException(nameof(langService));
            _reviewAPIOptions = reviewAPIOptions;
        }

        public async Task<IActionResult> GetDetail(int contactId, string PNR)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });



            try
            {

                var biresp = await _bookingClient.BookingImportFromBlankAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR, "", true, false);
                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);

                if (presp.HasError)
                {

                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 2
                        ,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = presp.Message
                    });
                }

                var reservationId = Convert.ToInt32(presp.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                var response = new BookingDetailResponse
                {
                    ID = basedetails.PNR,
                    StatusID = basedetails.StatusID,
                    CustomerNumber = basedetails.CustomerNumber,
                    ChangeDate = basedetails.ChangeDate,
                    BookingDate = basedetails.BookingDate,
                    CheckinDate = basedetails.CheckinDate,
                    CheckoutDate = basedetails.CheckoutDate,
                    Deposit = basedetails.Deposit,
                    AgentID = basedetails.AgentNr,
                    AgentName = basedetails.AgentName,
                    VoucherSendPossible = basedetails.VoucherSendPossible,
                    InvoiceSendPossible = basedetails.InvoiceSendPossible,
                    TicketSendPossible = basedetails.TicketSendPossible,
                    FlyCorendonURL = basedetails.FlyCorendonURL,
                };


                if (!string.IsNullOrEmpty(basedetails.CustomerNumber) && basedetails.CustomerNumber.Trim() != string.Empty)
                {
                    response.Amount = basedetails.Amount;
                    response.AmountOpen = basedetails.AmountOpen;
                }



                var hashText = $"{basedetails.PNR}{ basedetails.AgentNr}{ _brandService.Options.ParallelSystemsSecureKey}".ToSHA1();
                response.PaymentURL = $"{_brandService.Options.PaymentRootUrl}?PNR={basedetails.PNR}&AgentNr={basedetails.AgentNr}&shasign={hashText}&gopayment=1";

                var accoarr = await _bookingClient.BookingAccommodationsAsync(_brandService.Options.LoginName
                    , _brandService.Options.Password, reservationId, _brandService.BrandLanguage, false);
                response.Accommodations = new List<BookingAccommodation>();
                if (accoarr != null)
                {
                    foreach (var accoitem in accoarr)
                    {
                        BookingAccommodation acco = new BookingAccommodation();
                        acco.AccommodationID = accoitem.AccommodationCode;
                        acco.AccommodationLocation = accoitem.AccommodationLocation;
                        acco.AccommodationName = accoitem.AccommodationName;
                        acco.AccommodationStarRating = accoitem.AccommodationStarRating;
                        acco.AccommodationTopPicture = accoitem.AccommodationTopPicture;
                        acco.AccommodationTopPicture_Thumb = accoitem.AccommodationTopPicture_Thumb;
                        acco.AccommodationURL = accoitem.AccommodationURL;
                        acco.MapLatitude = accoitem.MapLatitude;
                        acco.MapLongitude = accoitem.MapLongitude;
                        acco.Rooms = new List<BookingAccommodationRoom>();
                        foreach (var room in await _bookingClient.AccommodationRoomsAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, _brandService.BrandLanguage, AllTypes: false, accoitem.AccommodationID))
                        {
                            var accRoom = new BookingAccommodationRoom();
                            accRoom.BoardingID = room.BoardingType;
                            accRoom.BoardingName = room.BoardingTypeName;
                            accRoom.CheckinDate = room.CheckinDate;
                            accRoom.CheckoutDate = room.CheckoutDate;
                            accRoom.Duration = room.Duration;
                            accRoom.RoomCount = room.RoomCount;
                            accRoom.RoomID = room.RoomType;
                            accRoom.RoomName = room.RoomTypeName;
                            accRoom.Passengers = new List<BookingGeneralPassenger>();

                            var accoPassengers = await _bookingClient.AccommodationPassengersAsync(_brandService.Options.LoginName, _brandService.Options.Password, room.ReservationAccommodationID, _brandService.BrandLanguage);

                            accRoom.Passengers.AddRange(accoPassengers);

                            acco.Rooms.Add(accRoom);
                        }
                        response.Accommodations.Add(acco);
                    }
                }

                var flightarr = basedetails.Flights;

                response.Flights = new List<BookingFlight>();
                if (flightarr != null)
                {
                    foreach (var bflight in flightarr)
                    {
                        BookingFlight flight = new BookingFlight();
                        flight.AirlineID = bflight.AirlineCode;
                        flight.AirlineName = bflight.AirlineName;
                        flight.ToAirport = bflight.ArrivalAirport;
                        flight.ToAirportName = bflight.ArrivalAirportName;
                        flight.ToCityName = bflight.ArrivalCityName;
                        flight.ArrivalDate = bflight.ArrivalDate;
                        flight.FromAirport = bflight.DepartureAirport;
                        flight.FromAirportName = bflight.DepartureAirportName;
                        flight.FromCityName = bflight.DepartureCityName;
                        flight.DepartureDate = bflight.DepartureDate;
                        flight.FlightNumber = bflight.FlightNumber;
                        flight.ReservationFlightID = bflight.ReservationFlightID;
                        flight.ViaAirportIATA = bflight.ViaAirportIATA;
                        flight.ViaAirportName = bflight.ViaAirportName;
                        flight.HasCasabInfo = bflight.HasCasabInfo;
                        flight.Segments = bflight.Segments;

                        flight.Passengers = new List<BookingGeneralPassenger>();

                        var flightPassengers = await _bookingClient.FlightPassengersAsync(_brandService.Options.LoginName, _brandService.Options.Password, flight.ReservationFlightID, _brandService.BrandLanguage);

                        flight.Passengers.AddRange(flightPassengers);

                        response.Flights.Add(flight);
                    }
                }

                if (response.Flights.Count > 0 && response.Accommodations.Count > 0)
                {
                    response.Type = 1;
                }
                else if (response.Flights.Count > 0)
                {
                    response.Type = 3;
                }
                else if (response.Accommodations.Count > 0)
                {
                    response.Type = 2;
                }
                if (response.Type == 1 || response.Type == 3)
                {
                    response.TravelFrom = response.Flights[0].FromAirportName;
                    response.TravelTill = response.Flights[0].ToAirportName;
                }
                else if (response.Accommodations.Count > 0)
                {
                    response.TravelFrom = "";
                    response.TravelTill = response.Accommodations[0].AccommodationLocation;
                }

                #region cancellation info
                response.CancelInfo = new BookingCancellationInfo();
                response.CancelInfo.CancelPossible = false;
                response.CancelInfo.WithoutPayment = false;
                response.CancelInfo.IBANRequired = false;
                response.IsAgentBooking = basedetails.IsAgentBooking;

                if (response.StatusID != 2)
                {
                    BookingCancelCheckResponse cancelcheck = await _bookingClient.CancelCheckAsync(_brandService.Options.LoginName, _brandService.Options.Password
                            , reservationId, _brandService.BrandLanguage);
                    if (cancelcheck.CancelPossible)
                    {
                        decimal PaidAmount = response.Amount - response.AmountOpen;
                        decimal WillPayAmount = cancelcheck.CancelAmount - PaidAmount;
                        if (WillPayAmount <= 0)
                        {
                            response.CancelInfo.CancelAmount = cancelcheck.CancelAmount;
                            response.CancelInfo.PaymentAmount = WillPayAmount;
                            response.CancelInfo.CancelPossible = true;
                            response.CancelInfo.WithoutPayment = true;
                            if (WillPayAmount < 0)
                            {
                                response.CancelInfo.IBANRequired = true;

                                if (!string.IsNullOrEmpty(response.CustomerNumber))
                                {
                                    var contactInfo = await _contactClient.BlankContactInfoAsync(_brandService.Options.LoginName, _brandService.Options.Password, _brandService.BrandLanguage, response.CustomerNumber);

                                    if (!string.IsNullOrEmpty(contactInfo.CustomerNo))
                                    {
                                        response.CancelInfo.IBAN = contactInfo.IBAN;
                                        response.CancelInfo.BIC = contactInfo.BIC;
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }
        public async Task<IActionResult> PaymentHistory(int contactId, string PNR)
        {


            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

            var prk = new PS.ContactService.ProcessResponse();
            try
            {

                //prk = await _contactClient.SessionControlAsync(_brandService.Options.LoginName, _brandService.Options.Password, SessionID: string.Empty);

                //if (prk.HasError)
                //{
                //    return new BadRequestObjectResult
                //    (
                //        new ErrorClass
                //        {
                //            ErrorCode = 5,
                //            Message = _langService.GetValueByKey("SESSIONID_CAN_NOT_FOUND"),
                //            DebugInfo = prk.Message
                //        }
                //    );
                //}

                //var request.ContactId = Convert.ToInt32(prk.IntParam1);

                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult
                    (
                        new ErrorResponse
                        {
                            ErrorCode = 2,
                            Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                            DebugInfo = presp.Message
                        }
                    );
                }

                var reservationId = Convert.ToInt32(presp.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                var paymentHistory = new List<BookingPaymentsPaymentResponse>();

                if (!string.IsNullOrEmpty(basedetails.CustomerNumber))
                {
                    if (basedetails.CustomerNumber.Trim() != "")
                    {
                        paymentHistory = (await _bookingClient.PaymentResponseListAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, _brandService.BrandLanguage)).ToList();
                    }
                }

                return new OkObjectResult(paymentHistory);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = err.ToString()
                    }
                );
            }
        }
        public async Task<IActionResult> AddBookingToContact(AddBookingRequest request)
        {

            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "PNR_MUSTBE_NUMERIC", "PNR_WASNOT_FOUND", "CONTACT_PNR_ALREADY_EXISTS", "AGENTNUMBER_WASNOT_FOUND_MSG", "INTERNAL_SERVER_ERROR", "CONTACT_PNRADDED_MSG" });


            try
            {
                if (!int.TryParse(request.ID, out int PNRNumeric))
                {
                    return new BadRequestObjectResult
                    (
                        new ErrorResponse
                        {
                            ErrorCode = 8,
                            Message = _langService.GetValueByKey("PNR_MUSTBE_NUMERIC"),
                            DebugInfo = ""
                        }
                    );
                }

                var pnrAddResponse = await _bookingClient.PNRAddToContactAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, PNRNumeric, request.Surname.Trim(), _brandService.BrandLanguage);

                if (pnrAddResponse.HasError)
                {
                    if (pnrAddResponse.ErrorCode == 1 || pnrAddResponse.ErrorCode == 3 || pnrAddResponse.ErrorCode == 5)
                    {
                        return new BadRequestObjectResult
                        (
                            new ErrorResponse
                            {
                                ErrorCode = 7
                                ,
                                Message = _langService.GetValueByKey("PNR_WASNOT_FOUND"),
                                DebugInfo = pnrAddResponse.ErrorMessage
                            }
                        );
                    }

                    if (pnrAddResponse.ErrorCode == 4)
                    {
                        return new BadRequestObjectResult
                         (
                            new ErrorResponse
                            {
                                ErrorCode = 6,
                                Message = _langService.GetValueByKey("CONTACT_PNR_ALREADY_EXISTS"),
                                DebugInfo = pnrAddResponse.ErrorMessage
                            }
                         );
                    }

                    if (pnrAddResponse.ErrorCode == 6)
                    {
                        return new BadRequestObjectResult
                        (
                           new ErrorResponse
                           {
                               ErrorCode = 9,
                               Message = _langService.GetValueByKey("AGENTNUMBER_WASNOT_FOUND_MSG"),
                               DebugInfo = pnrAddResponse.ErrorMessage
                           }
                        );

                    }

                    return new BadRequestObjectResult
                    (
                       new ErrorResponse
                       {
                           ErrorCode = 2,
                           Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                           DebugInfo = pnrAddResponse.ErrorMessage
                       }
                    );

                }

                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ID);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult
                    (
                        new ErrorResponse
                        {
                            ErrorCode = 2,
                            Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                            DebugInfo = presp.Message
                        }
                    );
                }

                var response = new AddBookingResponse();
                response.Message = _langService.GetValueByKey("CONTACT_PNRADDED_MSG");

                return new OkObjectResult(response);

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult
                (
                    new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = err.ToString()
                    }
                );
            }

        }

        public async Task<IActionResult> GetVoucher(int contactId, string PNR)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "VOUCHER_ISNOT_POSSIBLE", "PDFCREATE_ERROR_MSG" });

            try
            {
                var pResponse = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);

                if (pResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = pResponse.Message
                    });
                }

                var reservationId = Convert.ToInt32(pResponse.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                if (!basedetails.VoucherSendPossible)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("VOUCHER_ISNOT_POSSIBLE"),
                        DebugInfo = "Denied. VoucherSendPossible is false"
                    });
                }

                var voucherPdfResponse = await _bookingClient.GetPDFAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, contactId, _brandService.BrandLanguage, BookingOutputsSendType.Voucher);

                if (voucherPdfResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 7,
                        Message = _langService.GetValueByKey("PDFCREATE_ERROR_MSG"),
                        DebugInfo = voucherPdfResponse.ErrorText
                    });
                }

                var response = new PDFResponse();
                response.FileName = voucherPdfResponse.PDFFileName;
                response.FileContent = voucherPdfResponse.PDFFile;

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> GetTicket(int contactId, string PNR)
        {

            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "TICKET_ISNOT_POSSIBLE", "PDFCREATE_ERROR_MSG" });

            try
            {

                var pResponse = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);

                if (pResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = pResponse.Message
                    });
                }

                var reservationId = Convert.ToInt32(pResponse.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                if (!basedetails.TicketSendPossible)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("TICKET_ISNOT_POSSIBLE"),
                        DebugInfo = "Denied. TicketSendPossible is false"
                    });
                }

                var pdfResponse = await _bookingClient.GetPDFAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, contactId, _brandService.BrandLanguage, BookingOutputsSendType.Tickets);

                if (pdfResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 7, Message = _langService.GetValueByKey("PDFCREATE_ERROR_MSG"), DebugInfo = pdfResponse.ErrorText });
                }


                var relem = new PDFResponse();
                relem.FileName = pdfResponse.PDFFileName;
                relem.FileContent = pdfResponse.PDFFile;

                return new OkObjectResult(relem);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }

        }

        public async Task<IActionResult> InvoiceSendToEmail(int contactId, string PNR)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "INVOICE_ISNOT_POSSIBLE", "INVOCE_SEND_ERROR_MSG", "INVOCESEND_SUCCESSFULL" });


            var prk = new PS.ContactService.ProcessResponse();
            try
            {
                if (prk.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 5, Message = _langService.GetValueByKey("SESSIONID_CAN_NOT_FOUND"), DebugInfo = prk.Message });
                }

                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = presp.Message
                    });
                }
                var reservationId = Convert.ToInt32(presp.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                if (!basedetails.InvoiceSendPossible)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("INVOICE_ISNOT_POSSIBLE")
                        ,
                        DebugInfo = "Denied. InvoiceSendPossible is false"
                    });
                }

                var sendResponse = await _bookingClient.InvoiceTicketSendAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, contactId, _brandService.BrandLanguage, BookingOutputsSendType.Invoice);

                if (sendResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 7,
                        Message = _langService.GetValueByKey("INVOCE_SEND_ERROR_MSG")
                        ,
                        DebugInfo = sendResponse.ErrorText
                    });
                }


                var response = new InvoiceSendEmailResponse();
                response.Message = _langService.GetValueByKey("INVOCESEND_SUCCESSFULL");

                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> CancelBooking(CancelBookingRequest request)
        {
            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "BOOKINGCANCEL_NOTPOSSIBLE_MSG", "CANCEL_PAYMENT_REQUIRE_MSG", "IBAN_IS_MANDATORY", "IBAN_BIC_SAVE_ERROR", "CANCEL_AN_ERROR_OCCURED_MSG", "YOUR_BOOKING_IS_CANCELLED" });

            try
            {
                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ID);

                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 2,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = presp.Message
                    });
                }

                var reservationId = Convert.ToInt32(presp.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, reservationId, _brandService.BrandLanguage);

                var cancelcheck = await _bookingClient.CancelCheckAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, _brandService.BrandLanguage);
                if (!cancelcheck.CancelPossible)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6,
                        Message = _langService.GetValueByKey("BOOKINGCANCEL_NOTPOSSIBLE_MSG"),
                        DebugInfo = cancelcheck.ErrorMessage
                    });
                }

                decimal PaidAmount = basedetails.Amount - basedetails.AmountOpen;
                decimal WillPayAmount = cancelcheck.CancelAmount - PaidAmount;
                if (WillPayAmount > 0)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 7,
                        Message = _langService.GetValueByKey("CANCEL_PAYMENT_REQUIRE_MSG"),
                        DebugInfo = presp.Message
                    });
                }

                if (WillPayAmount < 0 && string.IsNullOrEmpty(request.IBAN)) //|| string.IsNullOrEmpty(request.BIC)))
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 8,
                        Message = _langService.GetValueByKey("IBAN_IS_MANDATORY"),
                        DebugInfo = presp.Message
                    });
                }

                if (WillPayAmount < 0)
                {
                    var refundResponse = await _bookingClient.RefundInsertAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, _brandService.BrandLanguage, -WillPayAmount, request.IBAN, request.BIC, "", false);

                    if (refundResponse.HasError)
                    {
                        return new BadRequestObjectResult(new ErrorResponse
                        {
                            ErrorCode = 9,
                            Message = _langService.GetValueByKey("IBAN_BIC_SAVE_ERROR"),
                            DebugInfo = presp.Message
                        });
                    }
                }

                var cancelResponse = await _bookingClient.CancelBookingAsync(_brandService.Options.LoginName, _brandService.Options.Password, reservationId, _brandService.BrandLanguage, true);

                if (cancelResponse.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 10,
                        Message = _langService.GetValueByKey("CANCEL_AN_ERROR_OCCURED_MSG"),
                        DebugInfo = presp.Message
                    });
                }


                var response = new CancelBookingResponse();
                response.Message = _langService.GetValueByKey("YOUR_BOOKING_IS_CANCELLED");
                return new OkObjectResult(response);
            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> CheckPnrPrivacy(CheckPnrPrivacyRequest request)
        {

            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR", "PNR_MUSTBE_NUMERIC", "CONTACT_PNR_NOTFOUND_MSG", "PLEASE_ENTERPNR_ANDCUSTNO_MSG", "BOOKING_NOT_ACCESSABLE_BECAUSE_IT_IS_NOT_BEEN_CONFIRMED_YET" }
            );

            try
            {
                if (request == null)
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = "Request cannot be null" });

                if (request.PNR.Trim().Length < 1 || request.Surname.Trim().Length < 1)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 6, Message = _langService.GetValueByKey("PLEASE_ENTERPNR_ANDCUSTNO_MSG"), DebugInfo = "PNR or surname cannot be null" });
                }

                if (request.PNR.Trim().Length < 3 || request.Surname.Trim().Length < 2)
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 7, Message = _langService.GetValueByKey("CONTACT_PNR_NOTFOUND_MSG"), DebugInfo = "PNR or surname the character limit error" });
                }

                if (!int.TryParse(request.PNR, out int PNRNumeric))
                {
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 8, Message = _langService.GetValueByKey("PNR_MUSTBE_NUMERIC"), DebugInfo = "The PNR must be numeric" });
                }



                var resObj = await _bookingClient.CheckPnrPrivacyAsync(_brandService.Options.LoginName, _brandService.Options.Password, request.ContactId, PNRNumeric, request.Surname, request.BirthDate, _brandService.BrandLanguage);

                if (resObj.HasError)
                {
                    if (resObj.ErrorCode == 1 || resObj.ErrorCode == 3)
                    {
                        return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 8, Message = _langService.GetValueByKey("CONTACT_PNR_NOTFOUND_MSG"), DebugInfo = string.Empty });
                    }

                    if (resObj.ErrorCode == 7)
                    {
                        return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 8, Message = _langService.GetValueByKey("BOOKING_NOT_ACCESSABLE_BECAUSE_IT_IS_NOT_BEEN_CONFIRMED_YET"), DebugInfo = string.Empty });
                    }

                    var errMsg = $"ErrorCode:{resObj.ErrorCode} | ReservationId: {resObj.ReservationID} | {resObj.ErrorMessage}";

                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 8, Message = errMsg });

                }

                return new OkObjectResult(new CheckPnrPrivacyResponse() { IsOkayPrivacyCheck = true, ReservationID = resObj.ReservationID });

            }
            catch (Exception err)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 2, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = err.ToString() });
            }
        }

        public async Task<IActionResult> BookingReview(int contactId, string PNR)
        {

            await _langService.GetValueList(new List<string> { "INVALID_PARAMETER_VAL_AUTH", "INVALID_PARAMETER_VAL_SESSIONID", "SESSIONID_CAN_NOT_FOUND", "INTERNAL_SERVER_ERROR" });

            try
            {

                var bookingImportResponse = await _bookingClient.BookingImportFromBlankAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR, string.Empty, true, false);

                var presp = await _bookingClient.ReservationIDFromPNRAsync(_brandService.Options.LoginName, _brandService.Options.Password, PNR);
                if (presp.HasError)
                {
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 6
                        ,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = presp.Message
                    });
                }

                var reservationId = Convert.ToInt32(presp.IntParam1);

                var basedetails = await _bookingClient.GetBookingDetailAsync(_brandService.Options.LoginName, _brandService.Options.Password, contactId, reservationId, _brandService.BrandLanguage);

                if (DateTime.Now <= basedetails.CheckoutDate)
                    new BadRequestObjectResult(new ErrorResponse { ErrorCode = 7, Message = string.Empty, DebugInfo = "It's not yet ready to review" });

                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                var client = new HttpClient();
                client.BaseAddress = new Uri(_reviewAPIOptions.Url);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(_reviewAPIOptions.Credential)));

                var apiResponse = await client.GetAsync($"reservation?bookingNumbers={PNR}");

                if (apiResponse.StatusCode != HttpStatusCode.OK)
                    return new BadRequestObjectResult(new ErrorResponse
                    {
                        ErrorCode = 8
                        ,
                        Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"),
                        DebugInfo = "The status of review api response is not 200"
                    });

                if (apiResponse.StatusCode != HttpStatusCode.OK)
                    throw new Exception();

                var responseAsString = apiResponse.Content.ReadAsStringAsync().Result;
                var response = Newtonsoft.Json.JsonConvert.DeserializeObject<List<ReviewApiResponseModel>>(responseAsString);


                if (response == null || response.Count == 0)
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 9, Message = string.Empty, DebugInfo = "Response is null or empty" });

                var reviewObj = response.FirstOrDefault();

                if (string.IsNullOrEmpty(reviewObj.ReviewUrl))
                    return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 10, Message = string.Empty, DebugInfo = "Review url is null or empty" });

                return new OkObjectResult(new BookingReviewResponseModel
                {
                    ReviewUrl = reviewObj.ReviewUrl
                });
            }
            catch (Exception ex)
            {
                return new BadRequestObjectResult(new ErrorResponse { ErrorCode = 11, Message = _langService.GetValueByKey("INTERNAL_SERVER_ERROR"), DebugInfo = ex.ToString() });
            }
        }
    }
}
