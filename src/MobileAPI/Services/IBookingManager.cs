﻿using Microsoft.AspNetCore.Mvc;
using MobileAPI.RequestModels;
using System.Threading.Tasks;

namespace MobileAPI.Services
{
    public interface IBookingManager
    {
        Task<IActionResult> GetDetail(int contactId, string PNR);
        Task<IActionResult> PaymentHistory(int contactId, string PNR);
        Task<IActionResult> AddBookingToContact(AddBookingRequest request);
        Task<IActionResult> GetVoucher(int contactId, string PNR);
        Task<IActionResult> GetTicket(int contactId, string PNR);
        Task<IActionResult> InvoiceSendToEmail(int contactId, string PNR);
        Task<IActionResult> CancelBooking(CancelBookingRequest request);
        Task<IActionResult> CheckPnrPrivacy(CheckPnrPrivacyRequest request);
        Task<IActionResult> BookingReview(int contactId, string PNR);
    }
}
