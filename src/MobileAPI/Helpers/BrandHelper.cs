﻿using MobileAPI.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Helpers
{
    public static class BrandHelper
    {
        public static string GetLanguageCodeByBrand(string brand)
        {
            var languageCode = string.Empty;
            switch (brand)
            {
                case Brands.MijnNL:
                    languageCode = LanguageCodesOfSystem.NL;
                    break;
                case Brands.MijnBE:
                    languageCode = LanguageCodesOfSystem.NL_BE;
                    break;
                case Brands.MonBE:
                    languageCode = LanguageCodesOfSystem.FR_BE;
                    break;
                default:
                    languageCode = LanguageCodesOfSystem.NL;
                    break;
            }

            return languageCode;
        }
        //public static bool IsBrandOnRequestCorrect(string brand)
        //{
        //    if (brand == Brands.MijnNL || brand == Brands.MijnBE || brand == Brands.MonBE)
        //        return true;
        //    return false;
        //}

        //public static string GetSearchUrlByBrand(string brand, VakantieSearchRequestModel request)
        //{
        //    try
        //    {
        //        switch (brand)
        //        {
        //            case Brands.MijnNL:
        //                return GetSearchUrlForNL(request);
        //            case Brands.MijnBE:
        //                return GetSearchUrlForBE(request);
        //            case Brands.MonBE:
        //                return GetSearchUrlForBEFR(request);
        //            default:
        //                return GetSearchUrlForNL(request);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //    }

        //    return GetSearchUrlForNL(request);

        //}

        //private static string GetSearchUrlForBEFR(VakantieSearchRequestModel request)
        //{
        //    var returnUrl = $"{Brands.BEFRWebSite}/voyages";

        //    if (request == null)
        //        return returnUrl;

        //    var queryString = string.Empty;

        //    if (request.Price.HasValue)
        //        queryString += $"adultPriceRange=0-{request.Price.Value}";


        //    if (request.DepartureDate.HasValue)
        //        queryString += !string.IsNullOrEmpty(queryString) ? $"&departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]" : $"departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]";

        //    if (!string.IsNullOrEmpty(queryString))
        //        returnUrl += $"?{queryString}";

        //    return returnUrl;
        //}

        //private static string GetSearchUrlForBE(VakantieSearchRequestModel request)
        //{
        //    var returnUrl = $"{Brands.BEWebSite}/vakanties";

        //    if (request == null)
        //        return returnUrl;

        //    var queryString = string.Empty;

        //    if (request.Price.HasValue)
        //        queryString += $"adultPriceRange=0-{request.Price.Value}";


        //    if (request.DepartureDate.HasValue)
        //        queryString += !string.IsNullOrEmpty(queryString) ? $"&departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]" : $"departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]";

        //    if (!string.IsNullOrEmpty(queryString))
        //        returnUrl += $"?{queryString}";

        //    return returnUrl;
        //}

        //private static string GetSearchUrlForNL(VakantieSearchRequestModel request)
        //{
        //    var returnUrl = $"{Brands.NLWebSite}/vakanties";

        //    if (request == null)
        //        return returnUrl;

        //    var queryString = string.Empty;

        //    if (request.Price.HasValue)
        //        returnUrl += $"/tot-{request.Price.Value}";

        //    if (request.DepartureDate.HasValue)
        //        queryString += !string.IsNullOrEmpty(queryString) ? $"&departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]" : $"departDate=[{request.DepartureDate.Value:yyMMdd},{request.DepartureDate.Value.AddDays(7):yyMMdd}]";

        //    if (!string.IsNullOrEmpty(queryString))
        //        returnUrl += $"?{queryString}";

        //    return returnUrl;
        //}
    }
}
