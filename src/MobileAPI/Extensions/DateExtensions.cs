﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Extensions
{
    public static class DateExtensions
    {
        public static string ToISOString(this DateTime? date, bool WithTime)
        {
            if (!date.HasValue)
                return string.Empty;

            var rvalue = $"{date.Value.Year}-{date.Value.Month.ToString().SetZeroToHead(2)}-{date.Value.Day.ToString().SetZeroToHead(2)}";

            if (WithTime)
            {
                rvalue += $"T{date.Value.Hour.ToString().SetZeroToHead(2)}:{date.Value.Minute.ToString().SetZeroToHead(2)}";
            }

            return rvalue;
        }
        public static string ToISOString(this DateTime date, bool WithTime)
        {
            
            var rvalue = $"{date.Year}-{date.Month.ToString().SetZeroToHead(2)}-{date.Day.ToString().SetZeroToHead(2)}";

            if (WithTime)
            {
                rvalue += $"T{date.Hour.ToString().SetZeroToHead(2)}:{date.Minute.ToString().SetZeroToHead(2)}";
            }

            return rvalue;
        }
    }
}
