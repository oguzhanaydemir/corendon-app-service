﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace MobileAPI.Extensions
{
    public static class StringExtensions
    {
        public static string ToSHA1(this string text)
        {
            var algorithm = new SHA1CryptoServiceProvider();            
            var encoding = System.Text.Encoding.GetEncoding("iso-8859-1");
            var byteHash = algorithm.ComputeHash(encoding.GetBytes(text));
            var delimitedHexHash = BitConverter.ToString(byteHash);
            
            return delimitedHexHash.Replace("-", "");
        }

        public static string SetZeroToHead(this string value, int size)
        {
            if (value.Length < size)
                return $"0{value}";
            return value;
        }

        public static string ToUpperWithReplace(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return value.Replace('i','ı').ToUpper();
        }

        public static string ToLowerWithReplace(this string value)
        {
            if (string.IsNullOrEmpty(value))
                return null;
            return value.Replace('I', 'İ').ToLower();
        }
    }
}
