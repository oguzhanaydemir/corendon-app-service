﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class CheckPnrPrivacyResponse
    {
        public bool IsOkayPrivacyCheck { get; set; }
        public int ReservationID { get; set; }
    }
}
