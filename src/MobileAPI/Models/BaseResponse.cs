﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class BaseResponse
    {
        public string Message { get; set; }
    }
}
