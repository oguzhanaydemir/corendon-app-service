﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class AccountEmailResponse
    {
        public bool Confirmed { get; set; }
        public int ContactEmailID { get; set; }
        public string TypeName { get; set; }
        public int EmailTypeID { get; set; }
        public string Email { get; set; }
    }
}
