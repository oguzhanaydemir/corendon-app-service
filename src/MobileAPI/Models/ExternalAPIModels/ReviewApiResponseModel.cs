﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models.ExternalAPIModels
{
    public class ReviewApiResponseModel
    {
        public string ReservationNumber { get; set; }
        public bool HasReview { get; set; }
        public string ReviewUrl { get; set; }
    }
}
