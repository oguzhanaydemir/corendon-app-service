﻿using System;

namespace MobileAPI.Models
{
    public class BaseReservation
    {
        public string ID { get; set; }
        public int StatusID { get; set; }
        public int Type { get; set; }
        public DateTime BookingDate { get; set; }
        public DateTime? CheckinDate { get; set; }
        public DateTime? CheckoutDate { get; set; }
        public decimal Amount { get; set; }
        public decimal AmountOpen { get; set; }
        public decimal Deposit { get; set; }
        public string AgentID { get; set; }
        public string AgentName { get; set; }
        public string TravelFrom { get; set; }
        public string TravelTill { get; set; }
        public bool IsAgentBooking { get; set; }
    }
    public class BaseFlight
    {
        public string AirlineID { get; set; }
        public string AirlineName { get; set; }
        public string FromAirportCode { get; set; }
        public string FromAirportName { get; set; }
        public string ToAirportCode { get; set; }
        public string ToAirportName { get; set; }
        public string FlightNumber { get; set; }
    }
    public class BaseAccommodation
    {
        public string AccommodationName { get; set; }
        public string AccommodationID { get; set; }
        public decimal AccommodationStarRating { get; set; }
        public string AccommodationTopPicture { get; set; }
        public string AccommodationTopPicture_Thumb { get; set; }
        public string AccommodationURL { get; set; }
    }

    public class BaseRoom
    {
        public DateTime CheckinDate { get; set; }
        public DateTime CheckoutDate { get; set; }
        public string RoomID { get; set; }
        public string RoomName { get; set; }
        public string BoardingID { get; set; }
        public string BoardingName { get; set; }
    }
}
