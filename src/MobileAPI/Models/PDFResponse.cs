﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class PDFResponse
    {
        public string FileName { get; set; }
        public byte[] FileContent { get; set; }
    }
}
