﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class UploadResponse
    {
        public string ProfilePicture { get; set; }
        public string ProfilePictureFullPath { get; set; }
    }
}
