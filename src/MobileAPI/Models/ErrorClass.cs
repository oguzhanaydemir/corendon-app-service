﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class ErrorResponse
    {
        [DataMember(Order = 1)]
        public int ErrorCode { get; set; }

        [DataMember(Order = 2)]
        public string Message { get; set; }
        
        [DataMember(Order = 3)]
        public string DebugInfo { get; set; }

    }
}
