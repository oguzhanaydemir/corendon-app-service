﻿using PS.BookingService;
using System;
using System.Collections.Generic;

namespace MobileAPI.Models
{
    public class BookingDetailResponse : BaseReservation
    {
        public DateTime? ChangeDate { get; set; }
        public List<BookingAccommodation> Accommodations { get; set; }
        public List<BookingFlight> Flights { get; set; }
        public string CustomerNumber { get; set; }
        public string PaymentURL { get; set; }
        public bool VoucherSendPossible { get; set; }
        public bool TicketSendPossible { get; set; }
        public bool InvoiceSendPossible { get; set; }
        public string FlyCorendonURL { get; set; }
        public BookingCancellationInfo CancelInfo { get; set; }
    }

    public class BookingCancellationInfo
    {
        public bool CancelPossible { get; set; }
        public bool WithoutPayment { get; set; }
        public bool IBANRequired { get; set; }
        public decimal CancelAmount { get; set; }
        public decimal PaymentAmount { get; set; }
        public string IBAN { get; set; }
        public string BIC { get; set; }
    }

    public class BookingAccommodation : BaseAccommodation
    {
        public List<BookingAccommodationRoom> Rooms { get; set; }
        public string AccommodationLocation { get; set; }
        public string MapLatitude { get; set; }
        public string MapLongitude { get; set; }
    }

    public class BookingAccommodationRoom : BaseRoom
    {
        public int Duration { get; set; }
        public int RoomCount { get; set; }
        public List<BookingGeneralPassenger> Passengers { get; set; }
    }

    public class BookingFlight : BaseFlight
    {
        public long ReservationFlightID { get; set; }
        public string FromAirport { get; set; }
        public string FromCityName { get; set; }
        public string ToAirport { get; set; }
        public string ToCityName { get; set; }
        public BookingFlightsFlightVia[] Segments { get; set; }
        public string ViaAirportIATA { get; set; }
        public string ViaAirportName { get; set; }
        public bool HasCasabInfo { get; set; }
        public List<BookingGeneralPassenger> Passengers { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
    }
}

