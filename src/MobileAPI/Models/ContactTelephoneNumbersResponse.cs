﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class ContactTelephoneNumbersResponse
    {
        public string TelephoneNumber { get; set; }
        public string ExtraNumber { get; set; }
        public string HomeNumber { get; set; }
    }
}
