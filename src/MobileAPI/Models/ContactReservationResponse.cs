﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MobileAPI.Models
{
    public class ContactReservationResponse : BaseReservation
    {
        public List<ContactReservationAccommodation> Accommodations { get; set; }
        public List<ContactReservationFlight> Flights { get; set; }
        public string Veranstalter { get; set; }
        public string UserID { get; set; }
        public string UserName { get; set; }
        public string FirstPassengerSurname { get; set; }
        public string StatusName { get; set; }
        public bool HasPrivacyCheck { get; set; }
    }


    public class ContactReservationAccommodation : BaseAccommodation
    {
        public List<ContactReservationRoom> Rooms { get; set; }
    }


    public class ContactReservationRoom : BaseRoom
    {
    }


    public class ContactReservationFlight : BaseFlight
    {
        public string DepartureDate { get; set; }
        public string ArrivalDate { get; set; }
    }
}
